import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenuRoutingModule } from './menu-routing.module';
import { MenuComponent } from './menu.component';
import {ReactiveFormsModule} from '@angular/forms';
import {NgSelectModule} from '@ng-select/ng-select';
import {WorkWithUsModule} from '../work-with-us.module';


@NgModule({
  declarations: [MenuComponent],
    imports: [
        CommonModule,
        MenuRoutingModule,
        ReactiveFormsModule,
        NgSelectModule,
        WorkWithUsModule
    ]
})
export class MenuModule { }
