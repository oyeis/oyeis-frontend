import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {FoodTypeService} from '../../services/food-type.service';
import {FoodTypeModel} from '../../models/foodType-model';
import {UtilityService} from '../../services/utility.service';
import {MenuService} from '../../services/menu.service';
import {AuthenticationService} from '../../services/authentication.service';
import {ActivatedRoute, Router} from '@angular/router';

class ImageSnippet {
  pending = false;
  status = 'init';
  constructor(public src: string, public file: File) {
  }
}
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  public menuForm: FormGroup;
  public cuisinesData: Array<FoodTypeModel>;
  public foodTypeData: Array<FoodTypeModel> ;
  public  foodTypeSelected = [];
  public  cuisinesTypeSelected: [];
  public uploadedImg: string;
  public isFileUploading = true;
  private selectedFile: ImageSnippet;
  public chefId: AuthenticationService;
  constructor(
      private fb: FormBuilder,
      private foodTypeService: FoodTypeService,
      private utilityService: UtilityService,
      private authenticationService: AuthenticationService,
      private menuService: MenuService,
      private activatedRoute: ActivatedRoute,
      private router: Router,
  ) {
    this.chefId = this.activatedRoute.snapshot.params.id;
   }

  ngOnInit(): void {
    this.menuFormInit();
    this.getCuisinesType();
    this.getFoodType();
  }
  menuFormInit() {
    this.menuForm =  this.fb.group({
      SHEFID: [this.chefId, Validators.required],
      MENUTITLE: ['', Validators.required],
      MENUTYPE: ['', Validators.required],
      CUISINESTYPE: ['', Validators.required],
      MENUDESC: ['', Validators.required],
      IMG: ['', Validators.required],
      MENUPRICE: ['', Validators.required],
      DELIVERYTYPE: ['1', Validators.required],
      ADDADDON: this.fb.array([]),
    });
  }
  get addAddon(): FormArray{
    return this.menuForm.get('ADDADDON') as FormArray;
  }
  addOnArray() {
    return this.fb.group({
      NAME: [''],
      PRICE: [''],
      DESC: [''],
    });
  }
  addRow() {
    this.addAddon.push(this.addOnArray());
  }
  deleteRow(i){
    this.addAddon.removeAt(i);
  }
  getCuisinesType = () => {
    this.foodTypeService.getAllCuisinesList('get_CuisineList')
        .subscribe((res) => {
          this.cuisinesData = [...res];
          console.log(this.cuisinesData);

        });
  }
  getFoodType = () => {
    this.foodTypeService.getAllFoodTypeList('get_FoodtypeList')
        .subscribe((res) => {
          this.foodTypeData = [...res];
          console.log(this.foodTypeData);
        });
  }

  onUploadImg(imageInput) {
    const file: File = imageInput.files[0];
    const reader = new FileReader();
    this.isFileUploading = true;
    // tslint:disable-next-line:no-shadowed-variable
    reader.addEventListener('load', ( event: any) => {
      this.selectedFile = new ImageSnippet(event.target.result, file);
      this.selectedFile.pending = true;
      const imageFor = 'menu_list_images';
      this.utilityService.uploadImage(this.selectedFile.file, imageFor)
          .subscribe((res) => {
            if (res.DATA) {
              console.log(res);
              this.isFileUploading = false;
              this.uploadedImg = res.DATA.IMAGE_URL;
              this.menuForm.controls.IMG.patchValue(this.uploadedImg);
            }
            console.log(this.uploadedImg);
          });
    });
    reader.readAsDataURL(file);
  }

  clearPreview() {
    this.isFileUploading = true;
    this.menuForm.controls.IMG.patchValue(null);
  }

  onSubmit() {
     if (this.menuForm.invalid){
         return;
     }
     this.menuService.createMenu(this.menuForm.value).subscribe((res) => {
       this.menuFormInit();
       this.router.navigate([`/menu/${this.chefId}`]);
    });
  }
}
