import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-chef-dashboard',
  templateUrl: './chef-dashboard.component.html',
  styleUrls: ['./chef-dashboard.component.scss']
})
export class ChefDashboardComponent implements OnInit {
  public userID: number;
  constructor(
      private activatedRoute: ActivatedRoute,
      public spinner: NgxSpinnerService,
  ) {
    this.userID = this.activatedRoute.snapshot.params.id;
  }

  ngOnInit(): void {
    this.spinner.hide();

  }

}
