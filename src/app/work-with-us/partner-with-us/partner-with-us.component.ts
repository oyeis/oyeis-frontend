import { NgxSpinnerService } from 'ngx-spinner';
import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';
import {Route, Router} from '@angular/router';

@Component({
  selector: 'app-partner-with-us',
  templateUrl: './partner-with-us.component.html',
  styleUrls: ['./partner-with-us.component.scss']
})
export class PartnerWithUsComponent implements OnInit {
  public isRegisterModal: boolean;
  public isLoginModalVisible: boolean;
  @Output() apiEndpoint: EventEmitter<any> = new EventEmitter();
  public routeUrl: any;
  constructor(
      private authentication: AuthenticationService,
      private router: Router,
      public spinner: NgxSpinnerService,

  ) {
    this.routeUrl = {
      routeUrl: 'login_chef',
      path: 'emailVerifyOtp',
    };
    // redirect to home if already logged in
   /* if (this.authentication.userValue) {
      this.router.navigate(['/create-profile']);
    }*/
  }

  ngOnInit(): void {
    this.spinner.hide();
  }

  onRegister() {
    this.isRegisterModal = true;
  }

  public hideModal($event: string) {
     if ($event === 'hideModal') {
      this.isLoginModalVisible = false;
      this.isRegisterModal = false;
    }
  }
}
