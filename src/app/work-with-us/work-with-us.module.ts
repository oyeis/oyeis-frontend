import { DataTablesModule } from 'angular-datatables';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PartnerWithUsComponent } from './partner-with-us/partner-with-us.component';
import {ShareableModule} from '../shareable/shareable.module';
import {RouterModule} from '@angular/router';
import { PartnerProfileComponent } from './partner-profile/partner-profile.component';
import { PartnerHeaderComponent } from './partner-header/partner-header.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NgxMaskModule} from 'ngx-mask';
import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown';
import { ChefDashboardComponent } from './chef-dashboard/chef-dashboard.component';
import { NewOrderComponent } from './new-order/new-order.component';
import { PendingOrderComponent } from './pending-order/pending-order.component';
import { CompleteOrderComponent } from './complete-order/complete-order.component';
import { CancelOrderComponent } from './cancel-order/cancel-order.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { OfferComponent } from './offer/offer.component';



@NgModule({
    declarations: [PartnerWithUsComponent, PartnerProfileComponent, PartnerHeaderComponent, ChefDashboardComponent, NewOrderComponent, PendingOrderComponent, CompleteOrderComponent, CancelOrderComponent, OfferComponent],
    exports: [
        PartnerHeaderComponent
    ],
    imports: [
        CommonModule,
        ShareableModule,
        RouterModule,
        ReactiveFormsModule,
        HttpClientModule,
        NgxMaskModule,
        NgMultiSelectDropDownModule,
        FormsModule,
        DataTablesModule,
        NgxSpinnerModule
        
    ]
})
export class WorkWithUsModule { }
