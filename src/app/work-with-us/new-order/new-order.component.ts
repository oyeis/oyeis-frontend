import { ActivatedRoute } from '@angular/router';
import { AuthenticationService } from './../../services/authentication.service';
import { MenuService } from './../../services/menu.service';
 import { Subject } from 'rxjs';
import { UtilityService } from 'src/app/services/utility.service';
import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-new-order',
  templateUrl: './new-order.component.html',
  styleUrls: ['./new-order.component.scss']
})
export class NewOrderComponent implements OnInit {
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};
  public chefId: number;
  public newOrder: Array<any>;
  public isOrderModalOpen: boolean;
  
  // We use this trigger because fetching the list of persons can be quite long,
  // thus we ensure the data is fetched before rendering
  dtTrigger: Subject<any> = new Subject<any>();
  ordersDetailArray: any;
  orderId: any;
  constructor(
      private menuService: MenuService,
      private authenticationService: AuthenticationService,
      private activatedRoute: ActivatedRoute,
      private utilityService: UtilityService,
      private spinner: NgxSpinnerService
  ) {
   this.chefId = this.activatedRoute.snapshot.params.id;
   }

   ngOnInit(): void {

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5
    };
    this.getOrderByChef();
  }
  getOrderByChef() {
    this.spinner.show();
    const data = {
      CHEF_ID: this.chefId,
    };
    this.menuService.getOrderByChef(data).subscribe((res) => {
      this.spinner.hide();
        this.newOrder = res.filter(order => order.ACCEPT_STATUS === 'pending');
        this.dtTrigger.next();
     });
  }

  openOrderModal = (orderDetail, orderId) => {
    this.isOrderModalOpen = true;
    console.log(orderDetail);
    this.ordersDetailArray = orderDetail;
    this.orderId = orderId
  }
  calculatePrice = (menuPrice, quantity) => {
   return (menuPrice*1) * (quantity * 1)
  }

  onChangeOrderStatus = (status) => {
    this.spinner.show();
    const data = {
      ACCEPT_STATUS: status,
      ORDER_ID: this.orderId
    };
    this.menuService.changeOrderStatus(data).subscribe((res) => {
      this.spinner.hide();
      this.rerender();
      this.getOrderByChef();
      this.isOrderModalOpen = false; 
       if(res.accept_status === 'yes'){
        this.utilityService.showAlert('Order Accepted Successfully', 'SUCCESS', 'SUCCESS');
      } else if (res.accept_status === 'no'){
        this.utilityService.showAlert('Order Rejected Successfully', 'SUCCESS', 'SUCCESS');
      }
 
    })
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
     });
  }
  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }
}
