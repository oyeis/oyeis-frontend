import { ActivatedRoute } from '@angular/router';
import { AuthenticationService } from './../../services/authentication.service';
import { MenuService } from './../../services/menu.service';
import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-complete-order',
  templateUrl: './complete-order.component.html',
  styleUrls: ['./complete-order.component.scss']
})
export class CompleteOrderComponent implements OnInit {
  public chefId: number;
  public menuListing: Array<any>;
  dtOptions: DataTables.Settings = {};
 
  // We use this trigger because fetching the list of persons can be quite long,
  // thus we ensure the data is fetched before rendering
  dtTrigger: Subject<any> = new Subject<any>();
  constructor(
      private menuService: MenuService,
      private authenticationService: AuthenticationService,
      private activatedRoute: ActivatedRoute,
  ) {
   this.chefId = this.activatedRoute.snapshot.params.id;
   }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5
    };
    this.getAllMenus();
  }
  getAllMenus() {
    const data = {
      SHEFID: this.chefId,
    };
    this.menuService.getAllMenuByUserID(data).subscribe((res) => {
        this.menuListing = res;
        this.dtTrigger.next();

    });
  }

}
