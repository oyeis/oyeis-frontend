import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';
import {PartnerService} from '../../services/partner.service';
import {ActivatedRoute} from '@angular/router';
import {OtpValidationService} from '../../services/otp-validation.service';
import { Spinner } from 'ngx-spinner/lib/ngx-spinner.enum';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-partner-header',
  templateUrl: './partner-header.component.html',
  styleUrls: ['./partner-header.component.scss']
})
export class PartnerHeaderComponent implements OnInit {
  public isUserLogin: boolean;
  public userID: number;
  public loggedInUserDetail: any;
  constructor(
    public otpValidationService: OtpValidationService,
    public authenticationService: AuthenticationService,
    public partnerService: PartnerService,
    public activatedRoute: ActivatedRoute,
  ) {
    this.userID = this.activatedRoute.snapshot.params.id;
  }

  ngOnInit(): void {
     // if (this.otpValidationService.userValue?.USERTYPE === 'chef') {
      if (this.otpValidationService.userValue?.USERTYPE === 'chef') {
          this.isUserLogin = true;
         
      }
      if (this.userID) {
        this.getPartnerData();
    }
   }

  getPartnerData = () => {
     const UID = {
      UID : this.userID,
    };
     this.partnerService.getUserData('get_Chef_Profile_Data', UID).subscribe((res) => {
       this.loggedInUserDetail = res;
     });
  }

  logoutUser() {
    this.otpValidationService.logout();
  }
}
