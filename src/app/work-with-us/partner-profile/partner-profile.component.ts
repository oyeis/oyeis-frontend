import { NgxSpinnerService } from 'ngx-spinner';
import {Component, HostListener, OnInit} from '@angular/core';
import { Form, FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService} from '../../services/authentication.service';
import {ActivatedRoute} from '@angular/router';
import {PartnerService} from '../../services/partner.service';
import {UtilityService} from '../../services/utility.service';
import {ParentProfileService} from '../../services/parent-profile.service';
import {appCommonConstant} from '../../constant/app-common-constant';
import {FoodTypeService} from '../../services/food-type.service';

class ImageSnippet {
  pending = false;
  status = 'init';
  constructor(public src: string, public file: File) {
  }
}
@Component({
  selector: 'app-partner-profile',
  templateUrl: './partner-profile.component.html',
  styleUrls: ['./partner-profile.component.scss']
})
export class PartnerProfileComponent implements OnInit {
   public appCommonConstant = appCommonConstant;
  partnerOutletInfo: FormGroup;
  public isFileUploading = true;
  public uploadedImg: string;
  private selectedFile: ImageSnippet;
  // tslint:disable-next-line:ban-types
  userID: Number;
  // tslint:disable-next-line:ban-types
  showFormNumber = 1;
  isSidebarSticky: boolean;
  private cuisineArray = [];
  private foodTypeArray = [];
  isKitchenLeftUploading: boolean;
  isSelfieWithKitchenUploading: boolean;
  isKitchenCenterUploading: boolean;
  isKitchenRightUploading: boolean;
  kitchenLeftImg: any;
  kitchenRightImg: any;
  kitchenCenterImg: any;
  selfieWithkitchenImg: any;
  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private authorization: AuthenticationService,
    private partnerService: PartnerService,
    private utilityService: UtilityService,
    private partnerProfileService: ParentProfileService,
    private foodTypeService: FoodTypeService,
    private spinner: NgxSpinnerService
  ) {
    this.userID = this.activatedRoute.snapshot.params.id;
  }
  cuisinesData = [];
  foodTypeData = [];
  selectedCuisines = [];
  selectedFoodType = [];
  dropdownSettings = {};



  ngOnInit(): void {
    this.getCuisinesType();
    this.getFoodType();
    this.partnerFormInit();
    this.getPartnerData();
    this.getOperationalHrs();
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'ID',
      textField: 'NAME',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 1,
      allowSearchFilter: true
    };

  }

  partnerFormInit() {
   this.partnerOutletInfo = this.fb.group({
     UID: [this.userID, Validators.required],
     FIRST_NAME: ['', Validators.required],
     LAST_NAME: ['', Validators.required],
     EMAIL: ['', Validators.required ],
     MOBILE: ['', Validators.required ],
     STATE: ['', Validators.required],
     CITY: ['', Validators.required],
     ADDRESS: ['', Validators.required],
     PINCODE: ['', Validators.required],
     CUISINE_ID: ['', Validators.required],
     FOOD_TYPE_ID: ['', Validators.required],
     PRIMARY_CONTACT_NO: ['', Validators.required],
     SECONDARY_CONTACT_NO: ['', Validators.required],
     OUTLET_NAME: ['', Validators.required],
     OUTLET_EMAIL: ['', Validators.required],
     OUTLET_ADDHAR_NUMBER: ['', [Validators.required]],
    //  OUTLET_NO: ['', Validators.required],
     ESTABLISHMENT_TYPE: ['', Validators.required],
     /*New Fields Required In API*/
     GENDER: ['', Validators.required],
     MENU_IMAGE:['', Validators.required],
     MORE_INFO: [''],
     PAN_CARD: ['', Validators.required],
     ACCOUNT_NUMBER: ['', Validators.required],
     ACCOUNT_NAME: ['', Validators.required],
     IFSC_CODE: ['', Validators.required],
     BRANCH_CODE:['', Validators.required],
     OPENS_AT: [''],
     CLOSE_AT: [''],
     TIME_SCHEDULED: this.fb.array([]),
     KITCHEN_LEFT_IMG: [''],
     KITCHEN_RIGHT_IMG: [''],
     KITCHEN_CENTER_IMG: [''],
     SELFIE_WITH_KITCHEN: [''],
   });
   }
  get timeSchedule(): FormArray{
    return this.partnerOutletInfo.get('TIME_SCHEDULED') as FormArray;
  }
  timeScheduleArray(element?: any){
    return this.fb.group({
      CHEF_ID: [element ? element.CHEF_ID : this.userID, Validators.required],
      TIMESCHEDULE_ID: [element ? element.ID : '', Validators.required],
      FROM_TIME: [element ? element.FROM_TIME : '', Validators.required],
      TO_TIME: [element ? element.TO_TIME : '', Validators.required],
      SELECTED_DAY: [element ? element.SELECTED_DATE : null, Validators.required],
     });
  }
  addRow() {
     this.timeSchedule.push(this.timeScheduleArray());
     console.log(this.timeSchedule.value)
   }
  deleteRow(i) {
    const deletingDataID = this.timeSchedule.controls[i].value.TIMESCHEDULE_ID
    console.log(this.timeSchedule.controls[i].value);
    if(deletingDataID){
      this.spinner.show();
      const data = {
        TIMESCHEDULE_ID: deletingDataID,
      }
      this.partnerProfileService.deletePartnerHrs('delete_timeschedule',data).subscribe((res) => {
        if(res.STATUS === 'SUCCESS'){
          this.timeSchedule.clear();
           this.getOperationalHrs();
        }
        this.spinner.hide();
      })
      return;
    }
    this.timeSchedule.removeAt(i);
  }
  onSubmit() {

    this.spinner.show();
    console.log(this.partnerOutletInfo.controls);
     if (this.partnerOutletInfo.invalid) {
      this.spinner.hide();
      return;
    }
    this.partnerProfileService.updateUserData('update_chef_profile', this.partnerOutletInfo.value)
        .subscribe((res) => {
           this.getPartnerData();
        });
  }
  getCuisinesType = () => {
    this.foodTypeService.getAllCuisinesList('get_CuisineList')
        .subscribe((res) => {
           this.cuisinesData = [...res];
        });
  }
  getFoodType = () => {
    this.foodTypeService.getAllFoodTypeList('get_FoodtypeList')
        .subscribe((res) => {
           this.foodTypeData = [...res];
        });
  }
  getPartnerData = () => {
    this.spinner.show();
    const UID = {
      UID : this.userID,
    };
    this.partnerService.getUserData('get_Chef_Profile_Data', UID).subscribe((res) => {
      if (res) {
         const commaToArrayCuisineId = res.CUISINE_ID.split(',');
        const commaToArrayFoodTypeId = res.FOOD_TYPE_ID.split(',');
        this.partnerOutletInfo.patchValue(res);
        if(res.MENU_IMAGE){
          this.isFileUploading = false;
          this.uploadedImg = res.MENU_IMAGE;
        }  else {
          this.isFileUploading = true;
        }
         if(res.KITCHEN_LEFT_IMG) {
          this.kitchenLeftImg = res.KITCHEN_LEFT_IMG; 
          this.isKitchenLeftUploading = false;
         } else {
           this.isKitchenLeftUploading = true;
         }
         if(res.KITCHEN_RIGHT_IMG) {
          this.kitchenRightImg = res.KITCHEN_RIGHT_IMG;
          this.isKitchenRightUploading = false;
        }  else {
          this.isKitchenRightUploading = true;
        }
         if(res.KITCHEN_CENTER_IMG) {
          this.kitchenCenterImg = res.KITCHEN_CENTER_IMG;
          this.isKitchenCenterUploading = false;
        }  else{
          this.isKitchenCenterUploading  = true;
        }
         if(res.SELFIE_WITH_KITCHEN) {
          this.selfieWithkitchenImg = res.SELFIE_WITH_KITCHEN;
          this.isSelfieWithKitchenUploading = false
        } else {
          this.isSelfieWithKitchenUploading = true;
        }

        console.log(this.isFileUploading)
        this.selectedCuisines = this.checkFoodTypeIsSelected(this.cuisinesData, commaToArrayCuisineId);
        this.selectedFoodType = this.checkFoodTypeIsSelected(this.foodTypeData, commaToArrayFoodTypeId);
        this.spinner.hide();
      }
    });
  }

  onSelectAll(items: any, itemType: string) {
    if (itemType === 'cuisines') {
      const tmp = [];
      this.partnerOutletInfo.controls.CUISINE_ID.patchValue('');
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < items.length; i++) {
       tmp.push(items[i].ID);
      }
      this.partnerOutletInfo.controls.CUISINE_ID.patchValue(tmp.join(','));
    }
    else {
      const tmp = [];
      this.partnerOutletInfo.controls.FOOD_TYPE_ID.patchValue('');
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < items.length; i++) {
        tmp.push(items[i].ID);
      }
      this.partnerOutletInfo.controls.FOOD_TYPE_ID.patchValue(tmp.join(','));
    }
  }
  onItemSelect(items: any, itemType: string) {
    if (itemType === 'cuisines') {
      this.cuisineArray.push(items.ID);
      this.partnerOutletInfo.controls.CUISINE_ID.patchValue(this.cuisineArray.join(','));
    } else {
      this.foodTypeArray.push(items.ID);
      this.partnerOutletInfo.controls.FOOD_TYPE_ID.patchValue(this.foodTypeArray.join(','));
    }
  }
  onDeSelect(items: any, itemType: string) {
    if (itemType === 'cuisines') {
      const tmp = [];
      const removeElement = this.removeDeselectElement(this.partnerOutletInfo.controls.CUISINE_ID.value, items.ID);
      this.partnerOutletInfo.controls.CUISINE_ID.patchValue('');
      removeElement.forEach((ele, index) => {
        tmp.push(ele);
      });
      this.partnerOutletInfo.controls.CUISINE_ID.patchValue(tmp.join(','));
    } {
      const tmp = [];
      const removeElement = this.removeDeselectElement(this.partnerOutletInfo.controls.FOOD_TYPE_ID.value, items.ID);
      this.partnerOutletInfo.controls.FOOD_TYPE_ID.patchValue('');
      removeElement.forEach((ele, index) => {
        tmp.push(ele);
      });
      this.partnerOutletInfo.controls.FOOD_TYPE_ID.patchValue(tmp.join(','));
    }
  }

  onDeSelectAll(items: any, itemType: string) {
   if (itemType === 'cuisines') {
     this.partnerOutletInfo.controls.CUISINE_ID.patchValue('');
   } else {
     this.partnerOutletInfo.controls.FOOD_TYPE_ID.patchValue('');
   }
  }
  removeDeselectElement = (numsArray, target) => {
    const filteredArray = numsArray.split(',');
    const tmpArray = [];
    // tslint:disable-next-line:prefer-for-of
    for (let index = 0; index < filteredArray.length; index++) {
      if (filteredArray[index] !== target) {
        tmpArray.push(filteredArray[index]);
      }
    }
    return tmpArray;
  }
  logOut() {
    this.authorization.logout();
  }
  /*isSelected True if comma separated value && cuisine Id is same. and same for fo0d type too*/
  checkFoodTypeIsSelected = ( normalArray, separatedArray) => {
    normalArray.map((ele) => {
        ele.isSelected = !!separatedArray.includes(ele.ID);
       });
     return normalArray.filter(array => array.isSelected === true);
  }

  scroll(el: HTMLElement) {
    el.scrollIntoView();
  }
  onUploadImg(imageInput, folderName) {
    this.spinner.show();
    const file: File = imageInput.files[0];
    const reader = new FileReader();
    this.isFileUploading = true;
    // tslint:disable-next-line:no-shadowed-variable
    reader.addEventListener('load', ( event: any) => {
      this.selectedFile = new ImageSnippet(event.target.result, file);
      this.selectedFile.pending = true;
      const imageFor = folderName.toLowerCase();
      this.utilityService.uploadImage(this.selectedFile.file, imageFor)
          .subscribe((res) => {
            if (res.DATA) {
               this.isFileUploading = false;
              switch (folderName) {
                case 'chef_menu_image':
                   this.partnerOutletInfo.controls.MENU_IMAGE.patchValue(res.DATA.IMAGE_URL); 
                   this.uploadedImg = res.DATA.IMAGE_URL; 
                   this.isFileUploading = false;
                  break;
                case 'KITCHEN_LEFT_IMG':
                  this.partnerOutletInfo.controls.KITCHEN_LEFT_IMG.patchValue(res.DATA.IMAGE_URL);
                  this.kitchenLeftImg = res.DATA.IMAGE_URL; 
                  this.isKitchenLeftUploading = false;
                  console.log(this.kitchenLeftImg)
                  break;
                case 'KITCHEN_RIGHT_IMG':
                  this.partnerOutletInfo.controls.KITCHEN_RIGHT_IMG.patchValue(res.DATA.IMAGE_URL);
                  this.kitchenRightImg = res.DATA.IMAGE_URL; 
                  this.isKitchenRightUploading = false;
                break;
                case 'KITCHEN_CENTER_IMG':
                  this.partnerOutletInfo.controls.KITCHEN_CENTER_IMG.patchValue(res.DATA.IMAGE_URL);
                  this.kitchenCenterImg = res.DATA.IMAGE_URL; 
                  this.isKitchenCenterUploading = false;
                break;
                case 'SELFIE_WITH_KITCHEN':
                  this.partnerOutletInfo.controls.SELFIE_WITH_KITCHEN.patchValue(res.DATA.IMAGE_URL);
                  this.selfieWithkitchenImg = res.DATA.IMAGE_URL; 
                  this.isSelfieWithKitchenUploading = false
                break;
                default:
                  break;
              }
              console.log(this.partnerOutletInfo.value)
             }
             this.spinner.hide();
           });
    });
    reader.readAsDataURL(file);
  }

  clearPreview(folderName) {
    console.log(folderName);
    switch (folderName) {
      case 'chef_menu_image':
        this.isFileUploading = true;
        this.partnerOutletInfo.controls.MENU_IMAGE.patchValue(null);        
        break;
      case 'KITCHEN_LEFT_IMG':
        this.isKitchenLeftUploading = true;
        break;
      case 'KITCHEN_RIGHT_IMG':
        this.isKitchenRightUploading = true;
      break;
      case 'KITCHEN_CENTER_IMG':
        this.isKitchenCenterUploading = true;
      break;
      case 'SELFIE_WITH_KITCHEN':
        this.isSelfieWithKitchenUploading = true;
      break;
      default:
        break;
    }
  }
  submitHrs = (i) => {
    this.spinner.show();
    // .chefId.patchValue(this.userID)
      this.partnerProfileService.updatePartnerHrs('add_edit_chef_timeschedule', this.timeSchedule.controls[i].value).subscribe((res) => {
      if(res.STATUS === 'SUCCESS') {
        if(this.timeSchedule.controls.length === 0){
          this.deleteRow(0);
        }
        this.timeSchedule.clear();
        this.getOperationalHrs();
      }
      this.spinner.hide();
    })
  }
  getOperationalHrs = () => {
    this.spinner.show();
   const data = {
      CHEF_ID: this.userID,
    }
    this.partnerProfileService.getPartnerHrs('getChefTimeScheduleList', data).subscribe((res) => {
       res.forEach(element => {
        this.timeSchedule.push(this.timeScheduleArray(element));
      });
      this.spinner.hide();
    })
  }
}
