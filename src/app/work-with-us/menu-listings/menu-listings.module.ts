import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenuListingsRoutingModule } from './menu-listings-routing.module';
import { MenuListingsHomeComponent } from './menu-listings-home/menu-listings-home.component';
import {WorkWithUsModule} from '../work-with-us.module';
import { DataTablesModule } from 'angular-datatables';


@NgModule({
  declarations: [MenuListingsHomeComponent],
  imports: [
    CommonModule,
    MenuListingsRoutingModule,
    WorkWithUsModule,
    DataTablesModule
  ]
})
export class MenuListingsModule { }
