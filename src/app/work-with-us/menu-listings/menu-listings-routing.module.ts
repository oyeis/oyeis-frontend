import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MenuListingsHomeComponent} from './menu-listings-home/menu-listings-home.component';


const routes: Routes = [
  {path: '', component: MenuListingsHomeComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MenuListingsRoutingModule { }
