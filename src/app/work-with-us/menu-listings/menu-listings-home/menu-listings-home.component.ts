import { Component, OnInit } from '@angular/core';
import {MenuService} from '../../../services/menu.service';
import {AuthenticationService} from '../../../services/authentication.service';
import {ActivatedRoute} from '@angular/router';
import { Subject } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-menu-listings-home',
  templateUrl: './menu-listings-home.component.html',
  styleUrls: ['./menu-listings-home.component.scss']
})
export class MenuListingsHomeComponent implements OnInit {
  public chefId: number;
  public menuListing: Array<any>;
  dtOptions: DataTables.Settings = {};
 
  // We use this trigger because fetching the list of persons can be quite long,
  // thus we ensure the data is fetched before rendering
  dtTrigger: Subject<any> = new Subject<any>();
  constructor(
      private menuService: MenuService,
      private authenticationService: AuthenticationService,
      private activatedRoute: ActivatedRoute,
      private spinner: NgxSpinnerService
  ) {
   this.chefId = this.activatedRoute.snapshot.params.id;
   }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5
    };
    this.getAllMenus();
  }
  getAllMenus() {
    this.spinner.show();
    const data = {
      SHEFID: this.chefId,
    };
    this.menuService.getAllMenuByUserID(data).subscribe((res) => {
        console.log(res);
        this.menuListing = res ? res: [];
        this.dtTrigger.next();
        this.spinner.hide();
    });
  }
}
