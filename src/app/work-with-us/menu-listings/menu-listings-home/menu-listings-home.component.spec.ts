import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuListingsHomeComponent } from './menu-listings-home.component';

describe('MenuListingsHomeComponent', () => {
  let component: MenuListingsHomeComponent;
  let fixture: ComponentFixture<MenuListingsHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuListingsHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuListingsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
