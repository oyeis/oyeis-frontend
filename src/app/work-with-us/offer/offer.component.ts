import { ActivatedRoute } from '@angular/router';
import { MenuService } from './../../services/menu.service';
 import { Subject } from 'rxjs';
import { UtilityService } from 'src/app/services/utility.service';
import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.scss']
})
export class OfferComponent implements OnInit {
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();

  public chefId: number;
  public cancelOrder: Array<any>;
  public isOrderModalOpen: boolean;
  ordersDetailArray: any;
  orderId: any;
  offerForm: FormGroup;
  chefOffersData: any;
  constructor(
      private menuService: MenuService,
      private activatedRoute: ActivatedRoute,
      private utilityService: UtilityService,
      private spinner: NgxSpinnerService,
      private fb: FormBuilder,
  ) {
   this.chefId = this.activatedRoute.snapshot.params.id;
   }

   ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 2
    };
    this.getChefOfferList();
    this.offerFormInit();
  }
  offerFormInit = () => {
    this.offerForm = this.fb.group({
      CHEF_ID: [this.chefId, Validators.required],
      TITLE: ['', Validators.required],
      MIN_PRICE: ['', Validators.required],
      DISCOUNT: ['', Validators.required],
      STATUS: ['1', Validators.required],
     });
  }
  

  openOrderModal = (orderDetail, orderId) => {
    this.isOrderModalOpen = true;
    console.log(orderDetail);
    this.ordersDetailArray = orderDetail;
    this.orderId = orderId
  }
  calculatePrice = (menuPrice, quantity) => {
   return (menuPrice*1) * (quantity * 1)
  }

  onSubmit = () => {
    this.spinner.show();
    if(this.offerForm.invalid){
      this.spinner.hide();
    }
    this.menuService.createChefOrder(this.offerForm.value).subscribe((res) => {
      this.spinner.hide();
      this.offerFormInit();
      this.getChefOfferList();
      this.isOrderModalOpen = false;
    })
  }
  getChefOfferList = () => {
    this.spinner.show();
    const data = {
      CHEF_ID: this.chefId
    }
    this.menuService.getChefOffersList(data).subscribe((res) => {
      this.chefOffersData = res;
      console.log(this.chefOffersData)
      this.spinner.hide();
    })
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
     });
  }
  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }
  editOffer = (offerData) => {
    this.offerForm.patchValue(offerData);
    this.isOrderModalOpen  = true;
  }
}

