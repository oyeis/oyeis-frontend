import { Component, OnInit } from '@angular/core';
import {OtpValidationService} from '../../services/otp-validation.service';
import {UtilityService} from '../../services/utility.service';

@Component({
  selector: 'app-profile-header',
  templateUrl: './profile-header.component.html',
  styleUrls: ['./profile-header.component.scss']
})
export class ProfileHeaderComponent implements OnInit {
  loggedInUserDetail: any;
   constructor(
        public otpValidationService: OtpValidationService,
        public utilityService: UtilityService,
  ) {
    this.routeUrl = {
      routeUrl: '/login_customer',
      path: 'emailVerifyOtp_customer',
    };
    this.isUserLogin = !!this.otpValidationService.userValue;
    if (this.otpValidationService.userValue) {
      this.userID = this.otpValidationService.userValue.ID;
    }
  }
   public isCityList = false;
   public userID: number;
   public isUserLogin: boolean;
  public routeUrl: any;
  public cartData: any;
  public cartItemPrice: any;
  public  isLoginModalVisible: boolean;
  public isSignUpModalVisible: boolean;

  ngOnInit(): void {
    this.cartItemPrice = [];
    this.getCartData();
    this.getLoggedInUserData();
  }
  getCartData = () => {
    this.utilityService.getItemData().subscribe((res) => {
        this.cartData = res;
     });
  }
  getLoggedInUserData = () => {
    const data = {
      UID: this.userID,
    }
    this.utilityService.getLoggedInUser(data).subscribe((res) => {
      if(res){
        this.loggedInUserDetail = res;
      }
    })
  }


  logOutUser() {
    this.otpValidationService.logout();
    this.isUserLogin = false;
  }
  public hideModal($event: string) {
    console.log($event);
    if ($event === 'showSignUpForm') {
      this.isSignUpModalVisible = true;
      this.isLoginModalVisible = false;
    }  else if ($event === 'showLoginForm') {
      this.isLoginModalVisible = true;
      this.isSignUpModalVisible = false;
    } else if ($event === 'closeModal') {
      this.isLoginModalVisible = true;
      this.isSignUpModalVisible = false;
    }  else {
      this.isSignUpModalVisible = false;
      this.isLoginModalVisible = false;
    }

  }

}

