import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HtCardLoaderComponent } from './ht-card-loader.component';

describe('HtCardLoaderComponent', () => {
  let component: HtCardLoaderComponent;
  let fixture: ComponentFixture<HtCardLoaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HtCardLoaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HtCardLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
