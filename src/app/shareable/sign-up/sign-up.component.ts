import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PartnerService} from '../../services/partner.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  @Output()
  isModalHide: EventEmitter<string> = new EventEmitter<string>();
  public signUpForm: FormGroup;
  public isButtonDisable: boolean;
  public isModalVisible: boolean;
  public isLoading: boolean;
  public userEmailId: any;
  public userData: any;
  constructor(
    private fb: FormBuilder,
    private partnerService: PartnerService,
  ) { }

  ngOnInit(): void {
    this.signUpFormInit();
  }
  signUpFormInit = () => {
    this.signUpForm = this.fb.group({
      FIRST_NAME: ['', Validators.required],
      LAST_NAME: ['', Validators.required],
      EMAIL: ['', Validators.required],
      MOBILE: ['', Validators.required],
      termsCondition: ['', Validators.required],
    });
  }

  checkTermsCondition(value: any) {
    this.isButtonDisable = !value.target.checked;
  }

  onSubmit() {
    this.isLoading  = true;
    if (this.signUpForm.invalid) {
      this.isLoading  = false;
      return true;
    }
    this.partnerService.regPartner('register_Customer', this.signUpForm.value)
        .subscribe((res) => {
          this.isLoading  = false;
          if (res.STATUS === 'SUCCESS') {
             this.userData = {
              path: 'emailVerifyOtp_customer',
              email: this.signUpForm.controls.EMAIL.value,
            };
          }
        });
  }
  public hideModal(eventType: string) {
    if (eventType === 'closeModal'){
      this.isModalHide.emit('hideModal');
    } else {
      this.isModalHide.emit('showLoginForm');
      this.userData = null;
    }
  }
}
