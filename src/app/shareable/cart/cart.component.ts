import { ParentProfileService } from './../../services/parent-profile.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Component, ElementRef, OnInit, Renderer2 } from '@angular/core';
import { UtilityService } from '../../services/utility.service';
import { AddressService } from '../../services/address.service';
import { OtpValidationService } from '../../services/otp-validation.service';
import { CartService } from '../../services/cart.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as confetti from 'canvas-confetti';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  public cartData: any[];
  public storedCartItem: any;
  public userId: number;
  public cartItemPrice: any[];
  public addressDetails: any;
  public selectedAddress: string;
  public couponCode: string;
  operationalHrs: any;
  selectedHrs: any;
  public isAddressModalOpen: boolean;
  public isCouponApplied = false;
  isCouponAppliedOnce: boolean;
  isLoginModalVisible: boolean;
  isRegisterModal: boolean;
  deliveryHrsType: any;
  customHrs: FormGroup;
  appliedOffer: any;
  timeType: string;
  constructor(
    private utilityService: UtilityService,
    private addressService: AddressService,
    private otpValidationService: OtpValidationService,
    private cartService: CartService,
    private router: Router,
    private fb: FormBuilder,
    private spinner: NgxSpinnerService,
    private partnerProfileService: ParentProfileService,
    private renderer2: Renderer2,
    private elementRef: ElementRef
  ) {
    this.userId = this.otpValidationService.userValue.ID;
    this.storedCartItem = JSON.parse(localStorage.getItem('cart'));
    if (this.storedCartItem) {
      this.storedCartItem.forEach((ele) => {
        this.utilityService.setItemData(ele);
      });
    }
    this.getDeliveryAddress();
    if (this.storedCartItem.length >= 1) {
      this.getOperationalHrs();
    }
  }

  ngOnInit(): void {
    this.getCartData();
    this.customHrsInit();
    this.getChefOfferList();
    if (localStorage.getItem('couponCode')) {
      this.couponCode = localStorage.getItem('couponCode');
      this.isCouponApplied = true;
      this.isCouponAppliedOnce = true;
    } else {
      this.couponCode = '';
      this.isCouponAppliedOnce = false;
    }

  }
  customHrsInit() { 
    this.customHrs = this.fb.group({
        date: [''],
        timeUntil: [''],
        timeFrom: [''],
    })
  }
  getCartData = () => {
    this.utilityService.getItemData().subscribe((res) => {
      this.cartData = res;
    });
  }
  inc(item: any) {
    item.QUANTIY += 1;
    localStorage.setItem('cart', JSON.stringify(this.cartData));
    this.getChefOfferList();
  }
  dec(item: any) {
    if (item.QUANTIY <= 1) {
      return;
    }
    item.QUANTIY -= 1;
    localStorage.setItem('cart', JSON.stringify(this.cartData));
    this.getChefOfferList();
  }

  removeItem(item: any) {
    this.utilityService.deleteItemData(item);
    localStorage.setItem('cart', JSON.stringify(this.cartData));
    this.getChefOfferList();
  }
  getCartItemPrice = (item?: any, offerPrice = 0) => {
    if (item.length >= 1) {
      const tmp = [];
      item.map((ele) => {
        tmp.push((ele.MENUPRICE * 1) * ele.QUANTIY);
      });
      this.cartItemPrice = tmp;
      const cartTotalAmount = this.cartItemPrice.reduce((a, b) => a + b);
      return (offerPrice === 0) ? cartTotalAmount : (cartTotalAmount - offerPrice);
    }
  }

  private getDeliveryAddress() {
    const data = {
      CUS_ID: this.userId,
    };
    this.addressService.getAllAddress(data).subscribe((res) => {
      if (res) {
        this.addressDetails = res;
      }
    });
  }

  checkOut() {
    if (!this.selectedHrs) {
      this.utilityService.showAlert('Please Select Delivery Hours', 'FAILURE', 'FAILURE');
      return;
    }
    const deliveryData = {
      CUS_ID: this.userId,
      PRODUCTDetail: JSON.parse(localStorage.getItem('cart')),
      TOTALAMOUNT: this.getCartItemPrice(this.cartData),
      PAYABLEAMOUNT: (this.getCartItemPrice(this.cartData) + 240),
      address: this.selectedAddress ? this.selectedAddress : null,
      DAY: this.selectedHrs.SELECTED_DATE,
      FROM: this.selectedHrs.FROM_TIME,
      TO: this.selectedHrs.TO_TIME,
      TIME_TYPE: this.timeType
    };
    if (deliveryData.address === null) {
      this.utilityService.showAlert('Please Select Delivery Address', 'FAILURE', 'FAILURE');
      return;
    }
   

    this.cartService.addToCart(deliveryData).subscribe((res) => {
      this.utilityService.cartData.next([]);
      localStorage.setItem('cart', '[]');
      window.location.href = `/dashboard/${this.userId}/order`
    });
  }

  setAddress(address: any) {
    this.selectedAddress = address;
  }
  setHours(hrs: any) {
    this.selectedHrs = hrs;
  }
  getOperationalHrs = () => {
    this.spinner.show();
    const data = {
      CHEF_ID: this.storedCartItem[0].SHEFID,
    }
    this.partnerProfileService.getPartnerHrs('getChefTimeScheduleList', data).subscribe((res) => {
      this.operationalHrs = res;
      this.spinner.hide();
    })
  }
  time24To12 = function (a) {
    //below date doesn't matter.
    return (new Date("1955-11-05T" + a + "Z")).toLocaleTimeString("bestfit", {
      timeZone: "UTC",
      hour12: !0,
      hour: "numeric",
      minute: "numeric"
    });
  };
  applyCoupon = () => {
    this.isCouponApplied = !this.isCouponApplied;
  }
  onSubmitCoupon = () => {
    localStorage.setItem('couponCode', this.couponCode);
    this.isCouponAppliedOnce = true;
    this.utilityService.showAlert('Coupon Applied Successfully', 'SUCCESS', 'SUCCESS');
  }
  clearCoupon = () => {
    localStorage.removeItem('couponCode');
    this.isCouponApplied = false;
    this.isCouponAppliedOnce = false;
    this.couponCode = '';
  }

  public hideModal($event: string) {
    if ($event === 'hideModal') {
      this.isLoginModalVisible = false;
      this.isRegisterModal = false;
    }
  }
  selectDeliveryHrs = (hrsType) => {
    console.log(hrsType)
    this.deliveryHrsType = hrsType;
    if(hrsType ==='RECOMMENDED'){
      this.customHrs.controls["date"].clearValidators();
      this.customHrs.controls["timeUntil"].clearValidators();
      this.customHrs.controls["timeFrom"].clearValidators();
      this.timeType = 'default'
    } else {
      this.customHrs.controls["date"].setValidators([Validators.required]);
      this.customHrs.controls["timeUntil"].setValidators([Validators.required]);
      this.customHrs.controls["timeFrom"].setValidators([Validators.required]);
      this.timeType = 'custom';
      const customData = {
        SELECTED_DATE: '',
        FROM_TIME: '',
        TO_TIME: '',
      }
      customData.SELECTED_DATE =  this.customHrs.controls['date'].value;
      customData.FROM_TIME = this.customHrs.controls['timeFrom'].value;
      customData.TO_TIME = this.customHrs.controls['timeUntil'].value;
      this.selectedHrs = customData
    }
    this.customHrs.get("date").updateValueAndValidity();
    this.customHrs.get("timeUntil").updateValueAndValidity();
    this.customHrs.get("timeFrom").updateValueAndValidity();
  }
  getChefOfferList = () => {
    const data = {
      CHEF_ID: this.storedCartItem[0].SHEFID,
    }
    this.utilityService.getChefOfferList(data).subscribe(res => {
      console.log(res)
      const tmpOffersListing = res.map((item) => item.MIN_PRICE * 1) 
      const filterOfferListing = res.map((item) => {
        if((item.MIN_PRICE * 1) === this.findCLosestOffer(tmpOffersListing.sort(), this.getCartItemPrice(this.cartData)) ){
          item.isOfferApplied = true
        } else{
          item.isOfferApplied = false
        }
        return item
      });
      this.appliedOffer = filterOfferListing.find((ele) => ele.isOfferApplied === true);
      // if(this.appliedOffer){
      //   this.surprise();
      // }
     })
  }
 
  findCLosestOffer = (array, goal) => {
    const tmpArray = [];
     array.forEach((ele) => {
       if(ele <= goal){
          tmpArray.push(ele);
       }
     })
    return tmpArray.reduce(function(prev, curr) {
        return (Math.abs(curr - goal) <= Math.abs(prev - goal) ? curr : prev);
      });
  }
  calculateMinPrice = (minPrice, discount) => {
    return ((minPrice * 1) * (discount * 1))/ 100
 }
  // public surprise(): void {
  
  //   const canvas = this.renderer2.createElement('canvas');

  //   this.renderer2.appendChild(this.elementRef.nativeElement, canvas);

  //   const myConfetti = confetti.create(canvas, {
  //     resize: true, // will fit all screen sizes
  //     useWorker: true
  //   });
  //   var duration = 30 * 1000;
  //   var end = Date.now() + duration;
    
  //   (function frame() {
  //     // launch a few confetti from the left edge
  //     confetti({
  //       particleCount: 7,
  //       angle: 60,
  //       spread: 55,
  //       origin: { x: 0 }
  //     });
  //     // and launch a few from the right edge
  //     confetti({
  //       particleCount: 7,
  //       angle: 120,
  //       spread: 55,
  //       origin: { x: 1 }
  //     });
    
  //     // keep going until we are out of time
  //     if (Date.now() < end) {
  //       requestAnimationFrame(frame);
  //     }
  //   }());
    
  //   setTimeout(() => {
  //     myConfetti.reset();
  //   }, 200);
  // }
}
