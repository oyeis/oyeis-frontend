import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VtCardLoaderComponent } from './vt-card-loader.component';

describe('VtCardLoaderComponent', () => {
  let component: VtCardLoaderComponent;
  let fixture: ComponentFixture<VtCardLoaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VtCardLoaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VtCardLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
