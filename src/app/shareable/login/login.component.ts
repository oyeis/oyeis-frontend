import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService} from '../../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @Output()
  isModalHide: EventEmitter<string> = new EventEmitter<string>();
  @Input() apiEndpoint: any;
  public userEmailId: string;
  public userData: any;

  public loginForm: FormGroup;
  public isLoading: boolean;
  constructor(
    private fb: FormBuilder,
    private authenticationService: AuthenticationService,
  ) { }

  ngOnInit(): void {
    this.loginFormInit();
   }
  loginFormInit = () => {
    this.loginForm = this.fb.group({
      MOBILE_EMAIL: ['', Validators.required],
     });
  }


  public hideModal(eventType: string) {
    this.userEmailId = null;
    this.userData = null;
    if (eventType === 'closeModal'){
      this.isModalHide.emit('hideModal');
    } else {
      this.isModalHide.emit('showSignUpForm');
    }
  }

  onSubmit() {
    this.isLoading = true;
    if (this.loginForm.invalid) {
      this.isLoading = false;
      return;
    }
    this.authenticationService.login(this.apiEndpoint.routeUrl, this.loginForm.value).subscribe((res) => {
        this.isLoading = false;
        if (res.STATUS === 'SUCCESS'){
          localStorage.setItem('userEmailId', JSON.stringify(this.loginForm.value));
          this.userData = {
            path: this.apiEndpoint.path,
           email: this.loginForm.controls.MOBILE_EMAIL.value,
         };
       }
    });
  }
}
