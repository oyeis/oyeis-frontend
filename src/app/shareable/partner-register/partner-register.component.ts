import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService} from '../../services/authentication.service';
import {Router} from '@angular/router';
import {PartnerService} from '../../services/partner.service';

@Component({
  selector: 'app-partner-register',
  templateUrl: './partner-register.component.html',
  styleUrls: ['./partner-register.component.scss']
})
export class PartnerRegisterComponent implements OnInit {
  togglePassword: boolean;
  @Output() isModalHide: EventEmitter<string> = new EventEmitter<string>();
  @Output() emailID: EventEmitter<string> = new EventEmitter<string>();
  public userEmailId: string;
  public registerForm: FormGroup;
  public isLoading: boolean;
   public showOTPInput: boolean;
    public userData: any;
   constructor(
    private fb: FormBuilder,
    private authentication: AuthenticationService,
    private router: Router,
    private partnerService: PartnerService,
     ) {
   }

  ngOnInit(): void {
    this.partnerFormInit();
   }

  partnerFormInit = () => {
    this.registerForm = this.fb.group({
      // APP_KEY: [appData.apiKey, Validators.required],
      EMAIL: ['', Validators.required],
      MOBILE: ['', Validators.required],
      FIRST_NAME: ['', Validators.required],
      LAST_NAME: ['', Validators.required],
     });
  }


  public hideModal(eventType: string) {
    if (eventType === 'closeModal') {
      this.isModalHide.emit('hideModal');
    } else {
        this.userData = null;
    }
  }

  onSubmit() {
       this.isLoading  = true;
       if (this.registerForm.invalid) {
           this.isLoading  = false;
           return true;
       }
       this.partnerService.regPartner('register_chef', this.registerForm.value)
         .subscribe((res) => {
             this.isLoading  = false;
             if (res.STATUS === 'SUCCESS') {
                 this.userData = {
                     path: 'emailVerifyOtp',
                     email: this.registerForm.controls.EMAIL.value,
                 };
                }
    });
  }
}
