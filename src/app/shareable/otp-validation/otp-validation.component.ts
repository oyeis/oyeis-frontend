import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {OtpValidationService} from '../../services/otp-validation.service';
import {UtilityService} from '../../services/utility.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-otp-validation',
  templateUrl: './otp-validation.component.html',
  styleUrls: ['./otp-validation.component.scss']
})
export class OtpValidationComponent implements OnInit {
  @Input() emailID: string;
  @Input() userData: any;
  @Output() isModalHide: EventEmitter<string> = new EventEmitter();
  isLoading: boolean;
  otpForm: FormGroup;
  private downloadTimer: any;
  private isButtonEnable: boolean;
  private showOTPInput: boolean;
  public countDown: number;
  public endRoute: string;
  constructor(
      private fb: FormBuilder,
      private otpService: OtpValidationService,
      private utilityService: UtilityService,
      private router: Router,
  ) {
   }

  ngOnInit(): void {
  this.otpFormInit();
  this.otpCountdown();
  }

  otpFormInit = () => {
    this.otpForm = this.fb.group({
      EMAIL: [this.userData.email, Validators.required],
      OTP: ['', Validators.required]
    });
  }


  public hideModal(eventType: string) {
     this.isModalHide.emit('hideModal');
  }
    verifyOTP(value: string) {
      this.otpForm.controls.OTP.patchValue(value);
      this.otpService.sendOTP(this.userData.path, this.otpForm.value).subscribe((res) => {
           if (res.DATA.USERTYPE === 'chef'){
              this.router.navigate([`/chef-dashboard/${res.DATA.ID}`]);
          } else {
              this.router.navigate([`/dashboard/${res.DATA.ID}`]);
          }
          this.utilityService.hideLoader();
      });
  }

    otpCountdown() {
     let  timeLeft = 60;
     this.downloadTimer = setInterval( () => {
      if (timeLeft === 0){
        clearInterval(this.downloadTimer);
        this.isButtonEnable = true;
      } else {
        this.isButtonEnable = false;
        this.countDown = timeLeft;
        this.showOTPInput = true;
      }
      timeLeft -= 1;
    }, 1000);
  }

  resendOTP() {
      const userEmail  = JSON.parse(localStorage.getItem('userEmailId'));
      const resendData = {
          EMAIL: userEmail.MOBILE_EMAIL,
      };
      clearInterval(this.downloadTimer);
      this.otpCountdown();
      console.log(resendData);
      this.otpService.resendOtp('resendEmailOtp', resendData).subscribe((res) => {
          console.log(res);
      });
  }
}
