import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodListingsAccCityComponent } from './food-listings-acc-city.component';

describe('FoodListingsAccCityComponent', () => {
  let component: FoodListingsAccCityComponent;
  let fixture: ComponentFixture<FoodListingsAccCityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoodListingsAccCityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodListingsAccCityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
