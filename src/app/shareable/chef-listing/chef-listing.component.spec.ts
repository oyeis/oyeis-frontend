import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChefListingComponent } from './chef-listing.component';

describe('ChefListingComponent', () => {
  let component: ChefListingComponent;
  let fixture: ComponentFixture<ChefListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChefListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChefListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
