import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MenuService } from '../../services/menu.service';
import { UtilityService } from '../../services/utility.service';
import { OtpValidationService } from '../../services/otp-validation.service';
import { Router } from '@angular/router';
import { FoodTypeService } from 'src/app/services/food-type.service';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-chef-listing',
  templateUrl: './chef-listing.component.html',
  styleUrls: ['./chef-listing.component.scss']
})
export class ChefListingComponent implements OnInit {
  public deliveryMenu = [];
  public takenAwayMenu = [];
  public isTakeAway = false;
  public storedCartItem: any;
  public city: string;
  public userId: string;
  public isUserLogin: boolean;
  public isRegisterModal: boolean;
  public isLoginModalVisible: boolean;
  public searchText: string;
  @Output() apiEndpoint: EventEmitter<any> = new EventEmitter();
  public routeUrl: any;
  public menuDetails: any;
  public showMenuDetailsModal: boolean;
  isSearchModalOpen: boolean;
  chefListing: any;
  isDeliverSystemHide: boolean;
  showSearchResult: boolean;
  searchedChef: any;
  showLoadingCard: boolean;
  constructor(

    private foodService: FoodTypeService,
    private spinner: NgxSpinnerService
  ) {
    this.routeUrl = {
      routeUrl: '/login_customer',
      path: 'emailVerifyOtp_customer',
    };

  }
  ngOnInit(): void {
    this.getAllCustomers();
  }

  getAllCustomers = () => {
    this.showLoadingCard = true;
    this.spinner.show();
    this.foodService.getAllCustomer().subscribe((res) => {
      this.showLoadingCard = false;
      this.chefListing = res;
      this.spinner.hide();
    });
  }
  convertStringToNumber = (input: string) => {
    if (!input) return 0;

    if (input.trim().length == 0) {
      return 0;
    }
    return Math.floor(Number(input));
  }
}
