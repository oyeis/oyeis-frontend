 import { FilterPipe } from './../helpers/filter.pipe';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { FoodListingsComponent } from './food-listings/food-listings.component';
import { FoodListingsAccCityComponent } from './food-listings-acc-city/food-listings-acc-city.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import {RouterModule} from '@angular/router';
import { PartnerRegisterComponent } from './partner-register/partner-register.component';
import {NgxMaskModule} from 'ngx-mask';
import { OtpValidationComponent } from './otp-validation/otp-validation.component';
import { NoPageFoundComponent } from './no-page-found/no-page-found.component';
import { CartComponent } from './cart/cart.component';
import { ProfileHeaderComponent } from './profile-header/profile-header.component';
import { NoItemFoundComponent } from './no-item-found/no-item-found.component';
import { ChefListingComponent } from './chef-listing/chef-listing.component';
import { VtCardLoaderComponent } from './vt-card-loader/vt-card-loader.component';
import { HtCardLoaderComponent } from './ht-card-loader/ht-card-loader.component';



@NgModule({
  declarations: [FilterPipe, HeaderComponent, FooterComponent, FoodListingsComponent, FoodListingsAccCityComponent, LoginComponent, SignUpComponent, PartnerRegisterComponent, OtpValidationComponent, NoPageFoundComponent, CartComponent, ProfileHeaderComponent, NoItemFoundComponent, ChefListingComponent, VtCardLoaderComponent, HtCardLoaderComponent],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        NgxMaskModule,
     ],
    exports: [HeaderComponent, FooterComponent, FoodListingsComponent, SignUpComponent, LoginComponent, PartnerRegisterComponent, ProfileHeaderComponent, ChefListingComponent, VtCardLoaderComponent, HtCardLoaderComponent]
})
export class ShareableModule { }
