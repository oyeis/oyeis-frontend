import { NgxSpinnerService } from 'ngx-spinner';
import { Component, HostListener, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { FoodTypeService } from 'src/app/services/food-type.service';
import {cityData} from '../../constant/city-data';
import {OtpValidationService} from '../../services/otp-validation.service';
import {PartnerService} from '../../services/partner.service';
import {UtilityService} from '../../services/utility.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public cityData = cityData;
  public isCityList = false;
  public searchText: string;
  public userID: number;
  public  isLoginModalVisible: boolean;
  public isSignUpModalVisible: boolean;
  public city: string;
  public isUserLogin: boolean;
  public routeUrl: any;
  public cartData: any;
  public cartItemPrice: any;
  public showAdditionalDetail: boolean;
  public isChefLogin: boolean;
  isSearchModalOpen: boolean;
  chefListing: any;
  showSearchResult: boolean;
  chefData: any;
  chefId: number;
  isDeliverSystemHide: boolean;
  searchedChef: number;
  loggedInUserDetail: any;
  constructor(
    private route: Router,
    public activatedRoute: ActivatedRoute,
    public otpValidationService: OtpValidationService,
    public partnerService: PartnerService,
    public utilityService: UtilityService,
    public foodService: FoodTypeService,
    private spinner: NgxSpinnerService
  ) {
    this.routeUrl = {
      routeUrl: '/login_customer',
      path: 'emailVerifyOtp_customer',
    };
    if (this.otpValidationService.userValue?.USERTYPE === 'customer'){
      this.isUserLogin = true;

    } else if (this.otpValidationService.userValue?.USERTYPE === 'chef') {
      this.isChefLogin = true;
    }
    this.city = this.activatedRoute.snapshot.paramMap.get('city');
    if (this.city) {
      this.searchText = this.city.toLowerCase().replace('-', ' ');
    }
    if (this.otpValidationService.userValue) {
      this.userID = this.otpValidationService.userValue.ID;
      this.getLoggedInUserData();

    }
   }

  ngOnInit(): void {
    this.cartItemPrice = [];
    this.getCartData();
    this.getAllCustomers();
  }
  getCartData = () => {
    this.utilityService.getItemData().subscribe((res) => {
        this.cartData = res;
     });
  }
  getLoggedInUserData = () => {
    const data = {
      UID: this.userID,
    }
    this.utilityService.getLoggedInUser(data).subscribe((res) => {
      if(res){
        this.loggedInUserDetail = res;
      }
    })
  }

   showCityList(searchText: string) {
    if (searchText) {
      if (searchText.split('').length >= 1){
        this.isCityList = true;
      }
    } else {
      this.isCityList = false;
    }
  }

  setSearchInput(cityName: string) {
    if (cityName) {
      this.searchText = cityName;
      this.isCityList = false;
    }
  }

  public hideModal($event: string) {
    console.log($event);
    if ($event === 'showSignUpForm') {
      this.isSignUpModalVisible = true;
      this.isLoginModalVisible = false;
    }  else if ($event === 'showLoginForm') {
      this.isLoginModalVisible = true;
      this.isSignUpModalVisible = false;
    } else if ($event === 'closeModal') {
      this.isLoginModalVisible = true;
      this.isSignUpModalVisible = false;
    }  else {
      this.isSignUpModalVisible = false;
      this.isLoginModalVisible = false;
    }

  }

  public findFoodAccToCity(searchText: string) {
    const tempSearchText = searchText.toLocaleLowerCase().replace(' ', '-');
    if (tempSearchText) {
      this.route.navigateByUrl(`city/${tempSearchText}`);
    }
  }


  logOutUser() {
     this.otpValidationService.logout();
     this.isUserLogin = false;
  }


  toggleAdditionalDetail() {
    this.showAdditionalDetail = ! this.showAdditionalDetail;
  }
  openSearchModal = () => {
     this.isSearchModalOpen = true;
    this.getAllCustomers();
    this.isDeliverSystemHide = true;
  }
  getAllCustomers = () => {
      this.foodService.getAllCustomer().subscribe((res) =>{
      this.chefListing  = res;
     });
  }
  searchChef = (searchText: string) => {
    this.showSearchResult = true;
    // const appBody =  document.getElementById('app-body');
      if(searchText.length >= 1){
        this.showSearchResult = true
     //  appBody.classList.add('overflowHidden')
     } else {
       this.showSearchResult = false;
     //  appBody.classList.remove('overflowHidden')
     }
   }

   getMenuByChef = (chefId) => {
    this.route.navigateByUrl(`/chef/${chefId}`)
    this.searchText = '';
    this.isSearchModalOpen = false;
    this.showSearchResult = false;
  }
}

