import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodListingsComponent } from './food-listings.component';

describe('FoodListingsComponent', () => {
  let component: FoodListingsComponent;
  let fixture: ComponentFixture<FoodListingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoodListingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodListingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
