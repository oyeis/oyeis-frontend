import {Component, EventEmitter, HostListener, Inject, OnInit, Output} from '@angular/core';
import {MenuService} from '../../services/menu.service';
import {UtilityService} from '../../services/utility.service';
import {OtpValidationService} from '../../services/otp-validation.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { FoodTypeService } from 'src/app/services/food-type.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { DOCUMENT } from '@angular/common';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-food-listings',
  templateUrl: './food-listings.component.html',
  styleUrls: ['./food-listings.component.scss']
})
export class FoodListingsComponent implements OnInit {
  public deliveryMenu = [];
  public isDelivery: boolean = true;
  public isReview: boolean;
  public takenAwayMenu = [];
  public isTakeAway = false;
  public  storedCartItem: any;
  public city: string;
  public userId: string;
  public isUserLogin: boolean;
  public isRegisterModal: boolean;
  public isLoginModalVisible: boolean;
  public searchText: string;
  @Output() apiEndpoint: EventEmitter<any> = new EventEmitter();
  public routeUrl: any;
  public menuDetails: any;
  public showMenuDetailsModal: boolean;
  isSearchModalOpen: boolean;
  chefListing: any;
  isDeliverSystemHide: boolean;
  showSearchResult: boolean;
  searchedChef: any;
  chefId: number;
  chefData: any;
  cartData: any[];
  isChefLogin: boolean;
  showLoadingCard: boolean;
  userReviewData: any;
  offerData: any;
  constructor(
      @Inject(DOCUMENT) private document: Document,
      private menuService: MenuService,
      private utilityService: UtilityService,
      private otpValidationService: OtpValidationService,
      private router: Router,
      private foodService: FoodTypeService,
      private spinner: NgxSpinnerService,
      private activatedRoute: ActivatedRoute
   ) {
     this.chefId = this.activatedRoute.snapshot.params.id;
    this.routeUrl = {
      routeUrl: '/login_customer',
      path: 'emailVerifyOtp_customer',
    };
    this.storedCartItem = JSON.parse(localStorage.getItem('cart'));
    if (this.storedCartItem) {
      this.storedCartItem.forEach((ele) => {
        this.utilityService.setItemData(ele, true);
      });
    }
    if (this.otpValidationService.userValue?.USERTYPE === 'customer'){
      this.isUserLogin = true;
      this.userId = this.otpValidationService.userValue.ID;
    } else if (this.otpValidationService.userValue?.USERTYPE === 'chef') {
      this.isChefLogin = true;
    }
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)  
    ).subscribe((event: NavigationEnd) => {
      if(event){
          this.chefId = this.activatedRoute.snapshot.params.id;
          this.getAllMenus();
          this.getAllCustomers();
          this.searchText = ''
      }
    });
  }
  ngOnInit(): void {
     this.getCartData();
     this.getAllMenus();
     this.getAllCustomers();
     this.getCustomerReview();
     this.getChefOfferList();
   }


    addItemInCart(item: any) {
      document.getElementById("cartListing").scrollIntoView();
      // If item chef Id is different then items stores in local storage then show error
      this.resetModal();
       item.STATUS = 'IN CART'; // appending a new key status
      item.QUANTIY = 1; // appending a new key status
      this.utilityService.setItemData(item);
    }
    addItemInFavourites = (data) => {
      this.resetModal();
      const itemData = {
          CUS_ID: this.userId,
          MENU_ID: data.MENU_ID,
          CHEF_ID: data.SHEFID
        };
       this.utilityService.addFavourite(itemData).subscribe((res) => {
        //  this.router.navigateByUrl(`/dashboard/${this.userId}/favourite`);
       });
    }

  public hideModal($event: string) {
     if ($event === 'hideModal') {
      this.isLoginModalVisible = false;
      this.isRegisterModal = false;
    }
  }

  viewMenusDetails(item: any) {
    this.menuDetails = item;
    this.showMenuDetailsModal = true;
   }

  resetModal() {
    this.showMenuDetailsModal = false;
  }
  getCartData = () => {
    this.utilityService.getItemData().subscribe((res) => {
      this.cartData = res;
     });
  }
  getCartItemPrice = (item?: any) => {
    if (item.length >= 1){
      const tmp = [];
      item.map((ele) => {
        tmp.push((ele.MENUPRICE * 1) * ele.QUANTIY);
      });
       return tmp.reduce( (a, b) => a + b );
    }
  }
  inc(item: any) {
    item.QUANTIY += 1;
    localStorage.setItem('cart', JSON.stringify(this.cartData));
   }
  dec(item: any) {
    if (item.QUANTIY <= 1){
      return;
    }
    item.QUANTIY -= 1;
    localStorage.setItem('cart', JSON.stringify(this.cartData));
  }

  removeItem(item: any) {
    this.utilityService.deleteItemData(item);
    localStorage.setItem('cart', JSON.stringify(this.cartData));
  }

  
  @HostListener('window:scroll', [])
  onWindowScroll() {
    const chefHeader = document.getElementById('chefProfile_sec');
    const foodListingWrapper = document.getElementById('food_listings');
    const sticky = chefHeader.offsetTop;
      if ( 100 < sticky) {
      chefHeader.classList.add('fixed_profile');
       foodListingWrapper.classList.add('managePadding')
    } else {
      chefHeader.classList.remove('fixed_profile');
      foodListingWrapper.classList.remove('managePadding')
    }
  }
  getAllCustomers = () => {
    this.spinner.show();
     this.foodService.getAllCustomer().subscribe((res) =>{
      this.chefData  = res.find(item => item.ID === this.chefId);
     });
  }
  getAllMenus() {
    this.showLoadingCard = true;
    if(this.showLoadingCard){
      this.deliveryMenu = null;
      this.takenAwayMenu = null;
    }
     const data = {
      SHEFID: this.chefId,
    };
    this.menuService.getAllMenuByUserID(data).subscribe((res) => {
      this.showLoadingCard = false;
       if(!this.showLoadingCard){
        this.deliveryMenu = res.filter((item) => item.DELIVERYTYPE === '1');
        this.takenAwayMenu = res.filter((item) => item.DELIVERYTYPE === '2');
      }
     });
  }
  convertStringToNumber = (input: string) => {
    if (!input) return 0;

    if (input.trim().length == 0) {
      return 0;
    }
    return Math.floor(Number(input));
  }
  getCustomerReview = () => {
    const data = {
      CHEF_ID: this.chefId,
    }
    this.utilityService.getCustomerReviewAccToChef(data).subscribe(data => {
       this.userReviewData = data;
    })
  }
  getChefOfferList = () => {
    const data = {
      CHEF_ID: this.chefId,
    }
    this.utilityService.getChefOfferList(data).subscribe(res => {
      this.offerData = res;
      console.log(res)
    })
  }
  calculateMinPrice = (minPrice, discount) => {
     return ((minPrice * 1) * (discount * 1))/ 100
  }
  
}
