import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  // tabType = 'orders';
  tabType = 'orders';
   constructor(
      private activatedRoute: ActivatedRoute,
      private spinner: NgxSpinnerService
  ) {
    if (window.location.pathname.split('/')[3] && window.location.pathname.split('/')[3] === 'favourite') {
      this.tabType = 'favourite';
    } else if (window.location.pathname.split('/')[3] && window.location.pathname.split('/')[3] === 'order') {
      this.tabType = 'orders';
    } else if (window.location.pathname.split('/')[3] && window.location.pathname.split('/')[3] === 'create-new-address') {
      this.tabType = 'address';
    }
  }

  ngOnInit(): void {
    this.spinner.hide();
  }

    showPages(pageTitle: string) {
        this.tabType = pageTitle;
    }
}
