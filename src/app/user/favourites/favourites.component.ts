import { filter } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import {MenuService} from '../../services/menu.service';
import {UtilityService} from '../../services/utility.service';
import {OtpValidationService} from '../../services/otp-validation.service';

@Component({
  selector: 'app-favourites',
  templateUrl: './favourites.component.html',
  styleUrls: ['./favourites.component.scss']
})
export class FavouritesComponent implements OnInit {
  public deliveryFoodList = [];
  public city: string;
  public userId: string;
  showLoadingCard: boolean;
  constructor(
      private utilityService: UtilityService,
      private otpValidationService: OtpValidationService,
  ) {
    this.userId = this.otpValidationService.userValue.ID;
  }
  ngOnInit(): void {
    this.getAllFavouriteMenu();
  }
  getAllFavouriteMenu() {
    this.showLoadingCard = true;
    const data = {
      CUS_ID: this.userId
    };
    this.utilityService.getFavourite(data).subscribe((res) => {
       this.showLoadingCard = false;
      this.deliveryFoodList = res.filter((ele) => ele.CHEF_ID !== "0");
    });
  }

  removeItemFromFavourite(item: any) {
    const data = {
      FAV_ID:  item.ID,
    };
    this.utilityService.deleteFavourite(data).subscribe((res) => {
      this.getAllFavouriteMenu();
    });
  }
  viewFavouritesItem = (item) => {
    console.log(item)
  }
}
