import { NgxSpinnerService } from 'ngx-spinner';
import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {OtpValidationService} from '../../services/otp-validation.service';
import {AddressService} from '../../services/address.service';
import {BehaviorSubject, Observable} from 'rxjs';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss']
})
export class AddressComponent implements OnInit {
  public isAddressCreated = true;
  public userId: string;
  public addressDetails: any;
   constructor(
      private otpValidationService: OtpValidationService,
      private addressService: AddressService,
      private spinner: NgxSpinnerService
  ) {
    this.userId = this.otpValidationService.userValue.ID;
    }

  ngOnInit(): void {
    this.getAllAddress();
  }
  getAllAddress = () => {
    this.spinner.show();
    const data = {
      CUS_ID: this.userId,
    };
    this.addressService.getAllAddress(data).subscribe((res) => {
     this.addressDetails = res;
     this.spinner.hide();
     });
  }
  deleteAddress = (item) => {
     const data = {
      ADDRESS_ID: item.ID
    };
     this.addressService.deleteAddress(data).subscribe((res) => {
      this.getAllAddress();
    });
  }
  AddressCreated($event: boolean) {
        this.isAddressCreated = $event;
        if (this.isAddressCreated){
          this.getAllAddress();
        }
  }

  editAddress(item: any) {
      this.addressService.setAddressData(item);
      this.isAddressCreated = false;
   }

    addAddress() {
        this.isAddressCreated = false;
        this.addressService.setAddressData(null);
    }
}
