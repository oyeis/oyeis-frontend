import { NgxSpinnerModule } from 'ngx-spinner';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import {ShareableModule} from '../shareable/shareable.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import {RouterModule} from '@angular/router';
import { PastOrderComponent } from './past-order/past-order.component';
import { FavouritesComponent } from './favourites/favourites.component';
import { ProfileComponent } from './profile/profile.component';
import {NgxMaskModule} from 'ngx-mask';
import {ReactiveFormsModule} from '@angular/forms';
import { AddressComponent } from './address/address.component';
import { PaymentComponent } from './payment/payment.component';
import { AddressModalComponent } from './address-modal/address-modal.component';



@NgModule({
  declarations: [HomeComponent, DashboardComponent, PastOrderComponent, FavouritesComponent, ProfileComponent, AddressComponent, PaymentComponent, AddressModalComponent],
    imports: [
        CommonModule,
        ShareableModule,
        RouterModule,
        NgxMaskModule,
        ReactiveFormsModule,
        NgxSpinnerModule
    ],
    exports: [AddressModalComponent, AddressComponent]
})
export class UserModule { }
