import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {validate} from 'codelyzer/walkerFactory/walkerFn';
import {AddressService} from '../../services/address.service';
import {OtpValidationService} from '../../services/otp-validation.service';
import {AddressComponent} from '../address/address.component';

@Component({
  selector: 'app-address-modal',
  templateUrl: './address-modal.component.html',
  styleUrls: ['./address-modal.component.scss']
})
export class AddressModalComponent implements OnInit {
  public addressForm: FormGroup;
  public isLoading = false;
  public userId: string;
  @Output()
  isAddressCreated: EventEmitter<boolean> = new EventEmitter<boolean>();
    private isUpdating: boolean;
  constructor(
      private fb: FormBuilder,
      private addressService: AddressService,
      private otpValidationService: OtpValidationService,
   ) {
      this.userId = this.otpValidationService.userValue.ID;
   }

  ngOnInit(): void {
       this.addFormInit();
       this.addressService.getAddressData().subscribe((res) => {
            if(res) {
                this.addressForm.patchValue(res);
                this.addressForm.controls.ADDRESS_ID.patchValue(res.ID);
                this.isUpdating = true;
            }
      });
  }

   addFormInit = () => {
    this.addressForm = this.fb.group({
      CUS_ID: [this.userId, Validators.required],
      CITY: ['', Validators.required],
      STATE: ['', Validators.required],
      COUNTRY: ['India', Validators.required],
      PINCODE: ['', Validators.required],
      LANDMARK: ['', Validators.required],
      ADDRESS: ['', Validators.required],
      ADDRESSTYPE: ['1', Validators.required],
      ADDRESS_ID: [''],
      DEFAULTSTATUS: ['yes', Validators.required],
     });
  }

    onSubmit() {
          this.isLoading = true;
          if (this.addressForm.invalid){
          this.isLoading = false;
          return;
        }
          if (this.isUpdating){
               this.addressService.editAddress(this.addressForm.value).subscribe((res) => {
                   this.isLoading = false;
                   this.generalSettings();
              });
          } else {
              this.addressService.addAddress('add_cus_address', this.addressForm.value).subscribe((res) => {
                  this.generalSettings();
                  this.isLoading = false;
              });
          }
    }

    hideModal() {
        this.isAddressCreated.emit(true);
    }
    generalSettings(){
        this.isLoading = false;
        this.isUpdating = false;
        this.addFormInit();
        this.isAddressCreated.emit(true);
    }

}
