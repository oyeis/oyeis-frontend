import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { UtilityService } from 'src/app/services/utility.service';
class ImageSnippet {
  pending = false;
  status = 'init';
  constructor(public src: string, public file: File) {
  }
}
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  public profileForm: FormGroup;
  userId: any;
  isFileUploading: boolean;
  userProfile: any;
  private selectedFile: ImageSnippet;
  constructor(
      private fb: FormBuilder,
      private utilityService: UtilityService,
      private activatedRoute: ActivatedRoute,
      private authenticationService: AuthenticationService,
      private spinner: NgxSpinnerService
      ) {
    this.userId = this.activatedRoute.snapshot.params.id;
   }

  ngOnInit(): void {
    this.profileFormInit();
    this.getLoggedInUserData();
  }
  profileFormInit = () => {
    this.profileForm  = this.fb.group({
      UID: ['', Validators.required],
      FIRST_NAME: ['', Validators.required],
      LAST_NAME: ['', Validators.required],
      EMAIL: ['', Validators.required],
      MOBILE: ['', Validators.required],
      PROFILE_IMG: ['']
     });
  }
  getLoggedInUserData = () => {
    this.spinner.show();
    const data = {
        UID: this.userId,
    }
    this.utilityService.getLoggedInUser(data).subscribe((res) => {
      this.spinner.hide();
       if(res.PROFILE_IMG){
        this.userProfile = res.PROFILE_IMG; 
        this.isFileUploading = false;
        console.log(this.userProfile)
      }
      this.profileForm.patchValue(res);
      this.profileForm.controls['UID'].patchValue(res.ID);
    });
  }
  onSubmit() {
    this.spinner.show();
    this.authenticationService.updateUserData(this.profileForm.value).subscribe((res) => {
      
      this.spinner.hide();
      if(res){
        this.getLoggedInUserData();
      }
    })
  }
  onUploadImg(imageInput, folderName) {
    this.spinner.show();
    const file: File = imageInput.files[0];
    const reader = new FileReader();
    this.isFileUploading = true;
    // tslint:disable-next-line:no-shadowed-variable
    reader.addEventListener('load', ( event: any) => {
      this.selectedFile = new ImageSnippet(event.target.result, file);
      this.selectedFile.pending = true;
      const imageFor = folderName.toLowerCase();
      this.utilityService.uploadImage(this.selectedFile.file, imageFor)
          .subscribe((res) => {
            if (res.DATA) {
               this.isFileUploading = false;
               this.profileForm.controls.PROFILE_IMG.patchValue(res.DATA.IMAGE_URL);
                  this.userProfile = res.DATA.IMAGE_URL; 
              }
             this.spinner.hide();
           });
    });
    reader.readAsDataURL(file);
  }
  clearPreview() {
    this.isFileUploading = true;
    this.profileForm.controls.PROFILE_IMG.patchValue(null);
  }
}
