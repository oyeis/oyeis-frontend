import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit } from '@angular/core';
import {OrderService} from '../../services/order.service';
import {ActivatedRoute} from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtilityService } from 'src/app/services/utility.service';
class ImageSnippet {
  pending = false;
  status = 'init';
  constructor(public src: string, public file: File) {
  }
}
@Component({
  selector: 'app-past-order',
  templateUrl: './past-order.component.html',
  styleUrls: ['./past-order.component.scss']
})
export class PastOrderComponent implements OnInit {
  private readonly userId: number;
  public orderDetail: any;
  orderDetailModal: any;
  showLoadingCard: boolean;
  completeOrderDetail: any;
  ratingIndex: any;
  feedbackForm: FormGroup;
  foodFeedback: any;
  isFileUploading = true;
  private selectedFile: ImageSnippet;
  reviewImg: any;

  constructor(
      public orderService: OrderService,
      public activatedRoute: ActivatedRoute,
      private fb: FormBuilder,
      private spinner: NgxSpinnerService,
      private utilityService: UtilityService,
  ) {
    this.userId = this.activatedRoute.snapshot.params.id;
  }
  feedbackArray = [
    {
      id: 1,
      title: 'Excellent',
      imgUrl:'../../../assets/img/star.png',
      rating: 5
    },
    {
      id: 2,
      title: 'Very Good',
      imgUrl:'../../../assets/img/smile.png',
      rating: 4
    },
    {
      id: 3,
      title: 'Average',
      imgUrl:'../../../assets/img/confused.png',
      rating: 3
    },
    {
      id: 4,
      title: 'Poor',
      imgUrl:'../../../assets/img/sad.png',
      rating: 2
    },
    {
      id: 5,
      title:  'Very Poor',
      imgUrl:'../../../assets/img/very-sad.png',
      rating: 1
    },
  ]

  ngOnInit(): void {
    this.getOrderDetails();
    this.feedbackFormInit();
  }
  feedbackFormInit = () => {
    this.feedbackForm = this.fb.group({
      CHEF_ID: ['', Validators.required],
      CUS_ID: [this.userId, Validators.required],
      MENU_ID: ['', Validators.required],
      RATING: ['', Validators.required],
      DESCRIPTION: ['', Validators.required],
      REVIEW_IMG: [''],
    })
  }
  getOrderDetails = () => {
    this.showLoadingCard = true;
    const data = {
      CUS_ID: this.userId,
    };
    this.orderService.getOrdersDetails(data).subscribe((res) => {
      this.showLoadingCard = false;
        if (res) {
          this.orderDetail = res;
         }
    });
  }
  showOrderDetail = (completeOrderDetail, selectedOrderDetail) => {
    this.orderDetailModal = selectedOrderDetail;
     this.completeOrderDetail = completeOrderDetail;
  }
  hideDetails = () => {
    this.orderDetailModal = null;
  }
  feedback = (food, chefId) =>{
     this.foodFeedback = food;
    this.feedbackForm.patchValue({
      CHEF_ID: chefId,
      MENU_ID: food.ID,
    });
    
    }
  markRating = (item, index) => {
    this.feedbackForm.controls.RATING.patchValue(item.rating);
    this.ratingIndex = index;
  }
  onUploadImg(imageInput, folderName) {
    this.spinner.show();
    const file: File = imageInput.files[0];
    const reader = new FileReader();
    this.isFileUploading = true;
    // tslint:disable-next-line:no-shadowed-variable
    reader.addEventListener('load', ( event: any) => {
      this.selectedFile = new ImageSnippet(event.target.result, file);
      this.selectedFile.pending = true;
      const imageFor = folderName.toLowerCase();
      this.utilityService.uploadImage(this.selectedFile.file, imageFor)
          .subscribe((res) => {
            if (res.DATA) {
               this.isFileUploading = false;
               this.feedbackForm.controls.REVIEW_IMG.patchValue(res.DATA.IMAGE_URL);
                  this.reviewImg = res.DATA.IMAGE_URL; 
              }
             this.spinner.hide();
           });
    });
    reader.readAsDataURL(file);
  }
  onSubmitFeedback = () => {
    this.spinner.show();
    this.utilityService.addFeedback(this.feedbackForm.value).subscribe((res) => {
      this.spinner.hide();
       this.foodFeedback = null;
    })
  }
  clearPreview() {
    this.isFileUploading = true;
    this.feedbackForm.controls.REVIEW_IMG.patchValue(null);
  }
}
