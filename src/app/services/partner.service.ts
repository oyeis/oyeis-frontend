import { Injectable } from '@angular/core';
import {ApiService} from './api.service';
import {catchError, map} from 'rxjs/operators';
import {UtilityService} from './utility.service';
import {of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PartnerService {

  constructor(
      private apiService: ApiService,
      private utilityService: UtilityService,
   )  {

  }

    /*Reg Partner*/
  regPartner(path, body){
    return   this.apiService.post(path, body).pipe(
        map(res => {
            console.log(res);
            if (res.STATUS === 'SUCCESS'){
                this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
                return res;
            } else {
                this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
                return of(false);
            }
         }),
        catchError(error => {
          console.log(error);
          this.utilityService.showAlert(error.MESSAGE, error.STATUS, error.STATUS);
          return of(false);
        })
    );
  }
  /*Get user detail by id*/
    getUserData( path, body){
         return this.apiService.post(path, body).pipe(
            map(res => {
                 if (res.STATUS === 'SUCCESS'){
                    res.DATA.role = 'Chef';
                    return res.DATA;
                } else {
                    this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
                    return of(false);
                }
            }),
         );
    }
}
