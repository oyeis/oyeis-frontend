import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {appData} from '../constant/app-data';
const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  origin: string;
  apiURL: string;

  constructor(
      private http: HttpClient
  ) {
    this.origin = window.location.origin;
    if (environment.production) {
      this.apiURL = apiUrl;
    } else {
      this.apiURL = apiUrl;
    }
    console.log(this.apiURL);
  }
  post(path: string, body: object = {}): Observable<any> {
    const formData = new FormData();
    // tslint:disable-next-line:forin
    for ( const key in body ) {
      formData.append(key, body[key]);
    }
    formData.append('APP_KEY', appData.apiKey);
    return this.http.post(
        `${this.apiURL}${path}`, formData);
  }
  postJSON(path: string, body: object = {}): Observable<any> {
    // @ts-ignore
    body.APP_KEY = appData.apiKey;
    return this.http.post(
        `${this.apiURL}${path}`, body);
  }
  get(path: string, body: object = {}): Observable<any> {
    // @ts-ignore
    body.APP_KEY = appData.apiKey;
    return this.http.get(
        `${this.apiURL}${path}`, body);
  }
}
