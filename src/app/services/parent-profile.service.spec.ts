import { TestBed } from '@angular/core/testing';

import { ParentProfileService } from './parent-profile.service';

describe('ParentProfileService', () => {
  let service: ParentProfileService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ParentProfileService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
