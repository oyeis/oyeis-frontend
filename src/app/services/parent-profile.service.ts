import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { of } from 'rxjs';
import { ApiService } from './api.service';
import { UtilityService } from './utility.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ParentProfileService {

  constructor(
    private apiService: ApiService,
    private utilityService: UtilityService,
  ) { }
  /*Update user detail by id*/
  updateUserData(path, body) {
    delete body.TIME_SCHEDULED;
    return this.apiService.postJSON(path, body).pipe(
      map(res => {
        if (res.STATUS === 'SUCCESS') {
          // console.log(localStorage.getItem('user'))
          res.DATA.role = 'Chef';
          this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
          return res.DATA;
        } else {
          this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
          return of(false);
        }
      }),
    );
  }
  updatePartnerHrs = (path, body) => {
    return this.apiService.postJSON(path, body).pipe(
      map(res => {
        if (res.STATUS === 'SUCCESS') {
          this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
          return res;
        } else {
          this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
          return of([]);
        }
      }),
    );
  }
  getPartnerHrs = (path, body) => {
    return this.apiService.postJSON(path, body).pipe(
      map(res => {
        if (res.STATUS === 'SUCCESS') {
         // this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
          return res.DATA;
        } else {
          this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
          return of([]);
        }
      }),
    );
  }
  deletePartnerHrs = (path, body) => {
    return this.apiService.postJSON(path, body).pipe(
      map(res => {
        if (res.STATUS === 'SUCCESS') {
       //  this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
          return res;
        } else {
          this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
          return of([]);
        }
      }),
    );
  }
}
