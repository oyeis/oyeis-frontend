import { Injectable, } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { catchError, map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { HttpClient, HttpErrorResponse, HttpEvent, HttpEventType } from '@angular/common/http';
import { BehaviorSubject, of, throwError } from 'rxjs';
import { appData } from '../constant/app-data';
import { ApiService } from './api.service';
const apiUrl = environment.apiUrl;
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {
  private apiURL: string;
  tempData = [];
  public cartData = new BehaviorSubject<any[]>([]);
  cartData$ = this.cartData.asObservable();
  constructor(
    private http: HttpClient,
    private toaster: ToastrService,
    private apiService: ApiService,
  ) {
    this.apiURL = apiUrl;

  }
  uploadImage(image: File, imageFor, imageType?: any) {
    const formData = new FormData();
    formData.append('FOLDER_NAME', imageFor);
    formData.append('IMAGE', image);
    formData.append('APP_KEY', appData.apiKey);
    const route = 'saveImageFiles';
    return this.http.post<any>(`${this.apiURL}` + route, formData, {
      reportProgress: true,
      observe: 'events'
    }).pipe(
      map(event => this.getEventMessage(event)),
      catchError(this.handleError)
    );
  }


  // to push new values
  setItemData(data, onLoad?: boolean) {

    const itemInCart = JSON.parse(localStorage.getItem('cart')) ? JSON.parse(localStorage.getItem('cart')) : [];
     if (itemInCart.length > 0) {
      if (itemInCart[0].SHEFID !== data.SHEFID) {
        Swal.fire({
          title: 'Do you want to change chef? Your cart item from previous chef will be replaced',
          showDenyButton: true,
          showCancelButton: false,
          confirmButtonText: 'Ok',
          denyButtonText: `Cancel`,
        }).then((result) => {
          /* Read more about isConfirmed, isDenied below */
          if (result.isConfirmed) {
             localStorage.removeItem('cart');
            this.tempData = [];
            this.tempData.push(data);
             this.cartData.next(this.tempData);
            localStorage.setItem('cart', JSON.stringify(this.tempData));
          //  this.showAlert('Item added in the cart.', 'SUCCESS', 'SUCCESS');
          }
        })
      } else {
        if (this.tempData.some(obj => obj.MENUTITLE === data.MENUTITLE)) {
          //this.showAlert('Item Already added in the cart', 'SUCCESS', 'SUCCESS');
        } else {
           this.tempData.push(data);
          this.cartData.next(this.tempData);
          localStorage.setItem('cart', JSON.stringify(this.tempData));
          if (!onLoad) {
          //  this.showAlert('Item added in the cart.', 'SUCCESS', 'SUCCESS');
          }
        }

      }

    } else {
       if (this.tempData.some(obj => obj.MENUTITLE === data.MENUTITLE)) {
        // this.showAlert('Item is already in the cart.', 'FAILURE', 'FAILURE');
        return;
      } else {
        this.tempData.push(data);
        this.cartData.next(this.tempData);
        localStorage.setItem('cart', JSON.stringify(this.tempData));
        if (!onLoad) {
        //  this.showAlert('Item added in the cart.', 'SUCCESS', 'SUCCESS');
        }
      }
    }

  }

  // to get Values
  getItemData() {
    return this.cartData$;
  }

  deleteItemData(data) {
    this.tempData = this.tempData.filter(element => {
      if (element.MENUTITLE === data.MENUTITLE) {
        element.status = '';
      }
      return element.MENUTITLE !== data.MENUTITLE;
    });
    this.cartData.next(this.tempData);
  }
  clearData() {
    this.tempData.forEach(element => {
      element.status = '';
    });
    this.tempData = [];
    this.cartData.next(this.tempData);
  }
  getEventMessage(event: HttpEvent<any>) {
    switch (event.type) {
      case HttpEventType.UploadProgress:
        return this.fileUploadProgress(event);
        break;
      case HttpEventType.Response:
        return this.apiResponse(event);
        break;
      default:
        return `wait...`;
    }
  }
  public fileUploadProgress(event) {
    const percentDone = Math.round(100 * event.loaded / event.total);
    return { status: 'progress', message: percentDone };
  }

  public apiResponse(event) {
    return event.body;
  }
  public handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    }
    return throwError(error);
  }

  /* Alert Utility */
  showAlert(message, alertTitle, alertType) {
    switch (alertType) {
      case 'SUCCESS':
        this.toaster.success(message, alertTitle);
        break;
      case 'FAILURE':
        this.toaster.error(message, alertTitle);
        break;
      default:
        this.toaster.warning('Something Went Wrong', 'Please Try Again Later');
        break;
    }
    return;
  }
  addFavourite = (body) => {
     return this.apiService.postJSON('add_favorite_menu', body).pipe(
      map(res => {
        if (res.STATUS === 'SUCCESS') {
          // this.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
          this.showAlert('Item added in the Favourites.', 'SUCCESS', 'SUCCESS');  
          return res.DATA;
        } else {
          this.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
          return of(false);
        }
      }),
    );
  }
  getFavourite = (body) => {
    return this.apiService.postJSON('get_favorites_menu_list', body).pipe(
      map(res => {
        if (res.STATUS === 'SUCCESS') {
          // this.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
          return res.DATA;
        } else {
          this.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
          return of(false);
        }
      }),
    );
  }

  deleteFavourite = (body) => {
    return this.apiService.postJSON('delete_favorite_menu', body).pipe(
      map(res => {
        if (res.STATUS === 'SUCCESS') {
          this.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
          return res.DATA;
        } else {
          this.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
          return of(false);
        }
      }),
    );
  }
  showLoader() {
    const loaderHtml = `
                <div class="boxes">
                  <div class="box">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                  </div>
                  <div class="box">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                  </div>
                  <div class="box">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                  </div>
                  <div class="box">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                  </div>
                </div>`;
    const loaderDiv = document.createElement('div') as HTMLElement;
    loaderDiv.setAttribute('class', 'd_flex');
    loaderDiv.innerHTML = loaderHtml;
    document.getElementById('loader-wrapper').append(loaderDiv);
  }
  hideLoader() {
    document.getElementById('loader-wrapper').style.display = 'none';
  }

  getLoggedInUser(body) {
    return this.apiService.postJSON('get_Cutomer_Profile_Data', body).pipe(
      map(res => {
        if (res.STATUS === 'SUCCESS') {
          // this.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
         // this.showAlert('Item added in the Favourites.', 'SUCCESS', 'SUCCESS');  
          return res.DATA;
        } else {
          //this.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
          return of(false);
        }
      }),
    );
  }
  addFeedback = (body) => {
    return this.apiService.postJSON('add_Review', body).pipe(
     map(res => {
       if (res.STATUS === 'SUCCESS') {
         // this.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
         this.showAlert('Review Added Successfully.', 'SUCCESS', 'SUCCESS');  
         return res.DATA;
       } else {
         this.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
         return of(false);
       }
     }),
   );
 }
 getCustomerReviewAccToChef = (body) => {
  return this.apiService.postJSON('allChefReviewList', body).pipe(
   map(res => {
     if (res.STATUS === 'SUCCESS') {
       // this.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
      // this.showAlert('Review Added Successfully.', 'SUCCESS', 'SUCCESS');  
       return res.DATA;
     } else {
       this.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
       return of(false);
     }
   }),
 );
}
getChefOfferList = (body) => {
  return this.apiService.postJSON('allChefOfferList', body).pipe(
   map(res => {
     if (res.STATUS === 'SUCCESS') {
       // this.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
      // this.showAlert('Review Added Successfully.', 'SUCCESS', 'SUCCESS');  
       return res.DATA;
     } else {
       this.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
       return of(false);
     }
   }),
 );
}
}
