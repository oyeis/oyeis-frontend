import { Injectable } from '@angular/core';
import {ApiService} from './api.service';
import {UtilityService} from './utility.service';
import {map} from 'rxjs/operators';
import {of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FoodTypeService {

  constructor(
      private apiService: ApiService,
      private utilityService: UtilityService,
  ) { }

  // tslint:disable-next-line:no-shadowed-variable
  getAllCuisinesList = (path: string) => {
   return  this.apiService.post(path).pipe(
        map((res) => {
          if (res.STATUS === 'SUCCESS'){
          //  this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
            return res.DATA;
          } else {
            this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
            return of([]);
          }
        })
    );
  }
  getAllFoodTypeList = (path: string) => {
   return  this.apiService.post(path).pipe(
        map((res) => {
          if (res.STATUS === 'SUCCESS'){
           // this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
            return res.DATA;
          } else {
            this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
            return of([]);
          }
        })
    );
  }
  getAllCustomer = () => {
    return  this.apiService.postJSON(`getChefList`).pipe(
         map((res) => {
            if (!res){
            this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
            return of([]);
           } else {
      
             return res.DATA;

           }
         })
     );
   }
}
