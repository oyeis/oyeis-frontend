import { Injectable } from '@angular/core';
import {ApiService} from './api.service';
import {UtilityService} from './utility.service';
import {map} from 'rxjs/operators';
import {of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  private apiRoute = 'place_order';
  constructor(
      private apiService: ApiService,
      private utilityService: UtilityService,
  ) { }
  addToCart = (data) => {
    return   this.apiService.postJSON(`${this.apiRoute}`, data).pipe(
        map(res => {
          if (res.STATUS === 'SUCCESS'){
            this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
            return res.DATA;
          } else {
            this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
            return of(false);
          }
        }),
    );
  }
}
