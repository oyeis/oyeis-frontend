import { Injectable } from '@angular/core';
import {ApiService} from './api.service';
import {catchError, map} from 'rxjs/operators';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {UtilityService} from './utility.service';
import {User} from '../models/user-model';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class OtpValidationService {
  private userSubject: BehaviorSubject<any>;
  public user: Observable<any>;
  constructor(
      private apiService: ApiService,
      private utilityService: UtilityService,
      private router: Router,
  ) {
    this.userSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('user')));
    this.user = this.userSubject.asObservable();
  }
  public get userValue(): any {
    return this.userSubject.value;
  }
  sendOTP = (path: string, data) => {
    localStorage.removeItem('user');
    this.userSubject.next(null);
    return   this.apiService.post(path, data).pipe(
        map(res => {
           if (res.STATUS === 'SUCCESS'){
            localStorage.setItem('user', JSON.stringify(res.DATA));
            this.userSubject.next(res.DATA);
            this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
            return res;
          } else {
            this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
            return of(false);
          }
        }),
        catchError(error => {
          console.log(error);
          this.utilityService.showAlert(error.MESSAGE, error.STATUS, error.STATUS);
          return of(false);
        })
    );
  }
  resendOtp = (path: string, data) => {
    return   this.apiService.post(path, data).pipe(
        map(res => {
           if (res.STATUS === 'SUCCESS'){
            localStorage.removeItem('user');
            this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
            return res;
          } else {
            this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
            return of([]);
          }
        })
    );
  }
    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('user');
        this.userSubject.next(null);
        this.router.navigate(['/']);
    }
}
