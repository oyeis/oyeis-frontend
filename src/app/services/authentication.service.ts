import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {User} from '../models/user-model';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {catchError, map} from 'rxjs/operators';
import {ApiService} from './api.service';
import {UtilityService} from './utility.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private userSubject: BehaviorSubject<any>;
  public user: Observable<any>;
  constructor(
    private router: Router,
    private http: HttpClient,
    private apiService: ApiService,
    private utilityService: UtilityService,
  ) {
    this.userSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')));
    this.user = this.userSubject.asObservable();
  }
  public get userValue(): any {
    return this.userSubject.value;
  }
  login(path: string, body) {
    return   this.apiService.post(path, body).pipe(
        map(res => {
            if (res.STATUS === 'SUCCESS'){
               this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
               return res;
          } else {
            this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
            return of(false);
          }
        }),
        catchError(error => {
          console.log(error);
          this.utilityService.showAlert(error.MESSAGE, error.STATUS, error.STATUS);
          return of(false);
        })
    );
   }

  logout() {
     // remove user from local storage to log user out
    localStorage.clear();
    this.userSubject.next(null);
    this.router.navigate(['/work-with-us']);
  }
  updateUserData = (body) => {
    return this.apiService.postJSON('update_customer_profile', body).pipe(
      map(res => {
        if (res.STATUS === 'SUCCESS') {
          // this.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
         this.utilityService.showAlert('Profile update Successfully', 'SUCCESS', 'SUCCESS');  
          return res.DATA;
        } else {
          this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
          return of(false);
        }
      }),
    );
  }
}
