import { Injectable } from '@angular/core';
import {ApiService} from './api.service';
import {UtilityService} from './utility.service';
import {map} from 'rxjs/operators';
import {BehaviorSubject, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AddressService {
    addressSubject = new BehaviorSubject<any>([]);
    address$ = this.addressSubject.asObservable();
  constructor(
      private apiService: ApiService,
      private utilityService: UtilityService,
  ) { }
    getAddressData() {
        return this.address$;
    }
  // tslint:disable-next-line:no-shadowed-variable
  addAddress = (path: string, body) => {
    return  this.apiService.postJSON(path, body).pipe(
        map((res) => {
          if (res.STATUS === 'SUCCESS'){
            this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
            return res;
          } else {
            this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
            return of([]);
          }
        })
    );
  }
  getAllAddress = (body) => {
    return  this.apiService.postJSON('get_all_address', body).pipe(
        map((res) => {
          if (res.STATUS === 'SUCCESS'){
            // this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
            return res.DATA;
          } else {
            this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
            return of([]);
          }
        })
    );
  }
  deleteAddress = (data) => {
    return  this.apiService.postJSON('delete_address', data).pipe(
        map((res) => {
          if (res.STATUS === 'SUCCESS'){
            // this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
            return res.DATA;
          } else {
            this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
            return of([]);
          }
        })
    );
  }
  editAddress = (data) => {
    return  this.apiService.postJSON('edit_cus_address', data).pipe(
        map((res) => {
          if (res.STATUS === 'SUCCESS'){
            // this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
            return res.DATA;
          } else {
            this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
            return of([]);
          }
        })
    );
  }
  setAddressData = (item) => {
      this.addressSubject.next(item);
  }
}
