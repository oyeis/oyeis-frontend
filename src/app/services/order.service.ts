import { Injectable } from '@angular/core';
import {ApiService} from './api.service';
import {UtilityService} from './utility.service';
import {catchError, map} from 'rxjs/operators';
import {of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
   constructor(
      private apiService: ApiService,
      private utilityService: UtilityService,
  )  {

  }

  /*Get User Details*/
  getOrdersDetails(body){
    return   this.apiService.postJSON(`getCustomerOrderList`, body).pipe(
        map(res => {
          console.log(res);
          if (res.STATUS === 'SUCCESS'){
           // this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
            return res.DATA;
          } else if (res === 'null') {
            this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
            return of(false);
          }
        }),
        catchError(error => {
          console.log(error);
       //   this.utilityService.showAlert(error.MESSAGE, error.STATUS, error.STATUS);
          return of(false);
        })
    );
  }

}
