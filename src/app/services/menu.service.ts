import { Injectable } from '@angular/core';
import {ApiService} from './api.service';
import {UtilityService} from './utility.service';
import {map} from 'rxjs/operators';
import {of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  private apiRoute = 'add_Listing';
  constructor(
      private apiService: ApiService,
      private utilityService: UtilityService,
  ) { }
  createMenu = (data) => {
   return   this.apiService.postJSON(`${this.apiRoute}`, data).pipe(
        map(res => {
          if (res.STATUS === 'SUCCESS'){
             this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
             return res.DATA;
          } else {
            this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
            return of(false);
          }
        }),
    );
  }
  getAllMenuByUserID = (data) => {
      return   this.apiService.postJSON(`get_menu_list_data`, data).pipe(
          map(res => {
               if (!res){
               //   this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
               //   this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
                  return of(false);
              } else {
                return res.DATA;
              }
          }),
      );
  }
  getAllActiveMenus = () => {
      return   this.apiService.postJSON(`get_all_menu_list`).pipe(
          map(res => {
              if (res){
                  if (res.STATUS === 'SUCCESS') {
              //    this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
                  return res.DATA;
                  }
              } else {
                  this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
                  return of(false);
              }
          }),
      );
  }
  getAllDeliveryMenu = () => {
      return   this.apiService.postJSON(`delivery_menu_list`).pipe(
          map(res => {
              if (res){
                  if (res.STATUS === 'SUCCESS') {
                      //    this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
                      return res.DATA;
                  }
              } else {
                  this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
                  return of(false);
              }
          }),
      );
  }
    getAllTakenMenu = () => {
        return   this.apiService.postJSON(`takeaway_menu_list`).pipe(
            map(res => {
                if (res){
                    if (res.STATUS === 'SUCCESS') {
                        //    this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
                        return res.DATA;
                    }
                } else {
                    this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
                    return of(false);
                }
            }),
        );
    }
    getOrderByChef = (data) => {
         return   this.apiService.postJSON(`getChefOrderList`, data).pipe(
            map(res => {
                if (res){
                     if (res.STATUS === 'SUCCESS') {
                        //    this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
                        return res.DATA;
                    }
                } else {
                    this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
                    return of(false);
                }
            }),
        );
    }
    changeOrderStatus = (data) => {
         return   this.apiService.postJSON(`orderAcceptChange`, data).pipe(
            map(res => {
                if (res){
                     if (res.STATUS === 'SUCCESS') {
                        //    this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
                        return res.DATA;
                    }
                } else {
                    this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
                    return of(false);
                }
            }),
        );
    }
    createChefOrder = (data) => {
        return   this.apiService.postJSON(`addChefOffer`, data).pipe(
            map(res => {
                if (res){
                     if (res.STATUS === 'SUCCESS') {
                        this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
                        return res.DATA;
                    }
                } else {
                    this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
                    return of(false);
                }
            }),
        );
    }
    getChefOffersList = (data) => {
        return   this.apiService.postJSON(`allChefOfferList`, data).pipe(
            map(res => {
                if (res){
                     if (res.STATUS === 'SUCCESS') {
                       // this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
                        return res.DATA;
                    }
                } else {
                    this.utilityService.showAlert(res.MESSAGE, res.STATUS, res.STATUS);
                    return of(false);
                }
            }),
        );
    }
    
}
