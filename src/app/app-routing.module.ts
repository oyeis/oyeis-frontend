import { OfferComponent } from './work-with-us/offer/offer.component';
import { FoodListingsComponent } from './shareable/food-listings/food-listings.component';
import { CancelOrderComponent } from './work-with-us/cancel-order/cancel-order.component';
import { PendingOrderComponent } from './work-with-us/pending-order/pending-order.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './user/home/home.component';
import {FoodListingsAccCityComponent} from './shareable/food-listings-acc-city/food-listings-acc-city.component';
import {PartnerWithUsComponent} from './work-with-us/partner-with-us/partner-with-us.component';
import {PartnerProfileComponent} from './work-with-us/partner-profile/partner-profile.component';
import {AuthGuard} from './helpers/auth.guard';
import {Role} from './models/role-model';
import {NoPageFoundComponent} from './shareable/no-page-found/no-page-found.component';
import {DashboardComponent} from './user/dashboard/dashboard.component';
import {ChefDashboardComponent} from './work-with-us/chef-dashboard/chef-dashboard.component';
import {CartComponent} from './shareable/cart/cart.component';
import { NewOrderComponent } from './work-with-us/new-order/new-order.component';
import { CompleteOrderComponent } from './work-with-us/complete-order/complete-order.component';


const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'city/:city', component: FoodListingsAccCityComponent},
  {path: 'work-with-us', component: PartnerWithUsComponent},
  {
    path: 'create-profile/:id',
    component: PartnerProfileComponent,
    canActivate: [AuthGuard],
   // data: { roles: [Role.Chef] }
  },
  {
    path: 'profile/:id',
    component: PartnerProfileComponent,
    canActivate: [AuthGuard],
   },
  {
    path: 'dashboard/:id',
    component: DashboardComponent,
    canActivate: [AuthGuard],
    },
    {
      path: 'dashboard/:id/create-new-address',
      component: DashboardComponent,
      canActivate: [AuthGuard],
      },
  {
    path: 'dashboard/:id/:orderType',
    component: DashboardComponent,
    canActivate: [AuthGuard],
    },
  {
    path: 'work-with-us',
    component: PartnerWithUsComponent,
     },
  {
    path: 'chef-dashboard/:id',
    component: ChefDashboardComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'cart',
    component: CartComponent,
   },
  {
    path: 'menu/:id',
    loadChildren: () => import('./work-with-us/menu-listings/menu-listings.module').then(m => m.MenuListingsModule),
    canActivate: [AuthGuard],
  },
  {
    path: 'manage-menu/:id',
    loadChildren: () => import('./work-with-us/menu/menu.module').then(m => m.MenuModule),
    canActivate: [AuthGuard],
  },
  {
    path: 'new-order/:id',
    component: NewOrderComponent,
    canActivate: [AuthGuard],
  }, {
    path: 'pending-order/:id',
    component: PendingOrderComponent,
    canActivate: [AuthGuard],
  }, {
    path: 'complete-order/:id',
    component: CompleteOrderComponent,
    canActivate: [AuthGuard],
  },{
    path: 'cancel-order/:id',
    component: CancelOrderComponent,
    canActivate: [AuthGuard],
  },{
    path: 'chef/:id',
    component: FoodListingsComponent,
  },{
    path: 'offer/:id',
    component: OfferComponent,
  },
 // {path: '**', component: NoPageFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
